<?php
/**
 * The footer
 *
 * Contains footer content and the closing of the
 * body and html.
 *
 * @package WordPress
 */
?>

<footer class="footer-main">
	<div class="footer-main__top">
		<div class="justifize">
			<div class="justifize__box">
				<a class="site-logo " href="/">
					<?php echo house_svg_icon( 'logo', 'logo' ); ?>
				</a>
			</div>
			<div class="justifize__box">
				<?php
				/**
				 * Get footer naivation
				 */
				get_template_part( 'partials/navigations/footer' );
				?>
			</div>
		</div>
	</div><!-- end of .footer-main__top -->

	<div class="footer-main__bottom">
		<div class="layout layout--middle layout--flush">
			<div class="layout__item large-and-up-1/3">
				<p class="mb0">
					<?php
						/**
						 * Get address
						 */
						if ( get_field( 'address', 'option' ) ) :
							the_field( 'address', 'option' );
						endif; // get_field( 'address', 'option' ?>

						<br />

					<?php
						/**
						 * Get email
						 */
						if ( get_field( 'email_address', 'option' ) ) : ?>
							<a href="mailto:<?php the_field( 'email_address', 'option' ); ?>">
								<?php the_field( 'email_address', 'option' ); ?>
							</a>
					<?php endif; // get_field( 'email_address', 'option' )

						/**
						 * Get phone number
						 */
						if ( get_field( 'phone_number', 'option' ) ) :
							echo ' / ';
							the_field( 'phone_number', 'option' );
						endif; // get_field( 'phone_number', 'option' )
					?>
				</p>
			</div><!-- end of .layout__item -->
			<div class="layout__item large-and-up-1/3">
				<p class="mb0 text-center">
					<?php echo get_bloginfo( 'name' );

					/**
					 * Get copyrights text
					 */
					if ( get_field( 'copyrights_text', 'option' ) ) :
						the_field( 'copyrights_text', 'option' );
					endif; // get_field( 'copyrights_text', 'option' )
					/**
					 * Get charity number
					 */
					if ( get_field( 'charity_number', 'option' ) ) :
						echo ', number ' . get_field( 'charity_number', 'option' ) . '.';
					endif; // get_field( 'charity_number', 'option' )
					?>
				</p>
			</div>
			<div class="layout__item large-and-up-1/3">
				<p class="mb0 theHouseLogo"><span class="inline-block">Made at</span>
					<a href="http://egzote.com/" target="_blank"><?php echo house_svg_icon( 'theHouseLogo' ); ?></a>
				</p>
			</div>
		</div><!-- end of .layout -->
	</div><!-- end of .footer-main__bottom -->
</footer>

<?php get_template_part( 'partials/meta/google-analytics' ); ?>
<?php get_template_part( 'partials/forms/login' ); ?>
<?php get_template_part( 'partials/forms/signup' ); ?>

<?php wp_footer(); ?>
</body>
</html>