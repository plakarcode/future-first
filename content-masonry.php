<?php
/**
 * Content masonry
 *
 * Template part for rendering content for masonry posts
 *
 * @package WordPress
 */
/**
 * Get the comments count.
 * @var int
 */
$comments_num = get_comments_number();
/**
 * Set the popular class for posts with more than 5 comments
 */
if ( $comments_num >= 5 ) {
	$popular = 'popular';
} else {
	$popular = '';
}
/**
 * Add extra post classes
 * @var array
 */
$classes = array(
	'grid-item',
	'transition',
	'all',
	$popular,
);
/**
 * Set class for grid item based on post format
 */
if ( get_post_format() === 'status' ) {
	$item_class = 'tweet';
} else {
	$item_class = 'news';
}

if ( ! is_front_page() ) {
	$item_content_class = 'grid-item__content';
} else {
	$item_content_class = '';
}

/**
 * If we have featured image, set it as background image
 */
if ( get_post_meta( get_the_ID(), 'tweet_media_image', true ) ) {
	/**
	 * Translators for custom_meta():
	 * custom field id, echo (true, false)
	 */
	$style = 'style="background-image: url( ' . custom_meta( 'tweet_media_image', false ) . ' );"';
} elseif ( has_post_thumbnail() ) {
	$style = 'style="background-image: url( ' . get_the_post_thumbnail_url( get_the_ID(), 'large' ) . ');"';
} else {
	$style = 'style="background-image: url(https://placehold.it/350x150);"';
}
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?> data-category="transition">

	<a href="" class="placeholder-image" <?php echo $style; ?>></a>

	<div class="grid-item__<?php echo $item_class; ?> <?php echo $item_content_class; ?>">
		<div class="grid-item__content-wrap">
			<div class="justifize">
				<div class="justifize__box">
					<time class="grid-item__date type-uppercase"><?php echo get_the_date( 'd F Y' ); ?></time>
				</div><!-- justifize__box -->
				<div class="justifize__box">
					<?php
						/**
						 * Twitter icon for tweets,
						 * category name for regular posts
						 */
						if ( get_post_format() === 'status' ) : ?>

							<a href="<?php custom_meta( 'tweet_permalink' ); ?>" target="_blank">
								<?php echo house_svg_icon( 'twitter', 'icon-twitter-news' ); ?>
							</a>

					<?php else : ?>
							<p class="grid-item__news--top type-uppercase"><?php first_category_name(); ?></p>
					<?php endif; // get_post_format() === 'status' ?>
				</div><!-- justifize__box -->
			</div><!-- justifize -->

			<hr>

			<?php
				/**
				 * Post format 'status', AKA Tweet
				 */
				if ( get_post_format() === 'status' ) :

					/**
					 * Avoid fatal error if buzz plugin
					 * is not activated.
					 */
					if ( function_exists( 'tweet_author' ) ) : ?>
						<h3>
							<?php echo tweet_author( 'name', 'twitter_data_wrap', 'option' ); ?>
							<span>&#064;<?php echo tweet_author( 'username', 'twitter_data_wrap', 'option' ); ?></span>
						</h3>
					<?php endif; // function_exists( 'tweet_author' ) ?>

					<div class="grid-item__post--content"><?php the_content(); ?></div>

					<?php if ( get_post_meta( get_the_ID(), 'tweet_media_image', true ) ) { ?>
						<img class="grid-item__img" src="<?php custom_meta( 'tweet_media_image' ); ?>" >
					<?php } ?>

					<div class="justifize grid-item__bottom-links">
						<div class="justifize__box">
							<?php // echo house_svg_icon( 'like', 'icon-like-news' ); ?>
						</div><!-- justifize__box -->
						<div class="justifize__box">
							<?php get_template_part( 'partials/content/share' ); ?>
						</div><!-- justifize__box -->
					</div><!-- justifize grid-item__bottom-links -->

			<?php
				/**
				 * Post format 'standard', AKA post
				 */
				else :

					/**
					 * Get post title
					 */
					get_template_part( 'partials/content/title-singular' ); ?>

					<div class="grid-item__post--content"><?php the_content(); ?></div>

					<?php
						/**
						 * Get featured image if one exists
						 */
						if ( has_post_thumbnail() ) :
							// translators: size, link to post, echo, class
							echo house_featured_image( 'medium_large', false, true, 'grid-item__img' );
						endif; // has_post_thumbnail()
					?>

					<div class="grid-item__bottom-links">
						<?php get_template_part( 'partials/content/share' ); ?>
					</div><!-- grid-item__bottom-links -->

			<?php endif; // get_post_format() === 'status' ?>

		</div><!-- grid-item__content-wrap -->
	</div><!-- grid-item__<?php echo $item_class; ?> <?php echo $item_content_class; ?> -->

</article><!-- #post -->