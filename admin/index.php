<?php
/**
 * Here include all admin/dashboard related functions
 *
 */
// general dashboard functions
include( get_template_directory() . '/admin/dashboard.php' );
// theme options
include( get_template_directory() . '/admin/theme-options.php' );