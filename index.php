<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 */
get_header();
global $wp_query; ?>

<div class="main-content">
	<div class="bgr-gray">
		<div class="container pb++">

			<?php get_template_part( 'partials/navigations/breadcrumbs'); ?>
			<?php get_template_part( 'partials/content/title-archive'); ?>

			<div class="layout">
				<div class="tabs tabs--magic mb filter-menu layout__item">
					<div id="tab-news" class="tabs__item">

						<?php get_template_part( 'partials/content/news-masonry'); ?>

					</div><!-- tabs__item -->
				</div><!-- tabs tabs--magic mb filter-menu -->
			</div><!-- layout -->

		</div><!-- container pb++ -->
	</div><!-- bgr-gray -->
</div><!-- main-content -->

<?php
	/**
	 * Get subscribe section
	 */
	get_template_part( 'partials/content/subscribe' );

get_footer();