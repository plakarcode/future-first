<?php
/**
 * The default template for displaying content.
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
		/**
		 * Get flexible content fields if any,
		 * otherwise get regular content
		 */
		if ( get_field( 'simple_content_fields' ) ) :

			get_template_part( 'partials/simple-flexible/sections' );

		endif; // get_field( 'content_fields' )
	?>

	<div class="container">
		<div class="story-main">
			<div class="justifize mt+">
				<div class="justifize__box">
					<?php if ( get_post_type() === 'post' ) : ?>
					<div class="categories">
						<span>Categories</span>
						<?php house_entry_taxonomies( 'category', '</li><li>', '' ); ?>
					</div>
					<?php endif; // get_post_type() === 'post' ?>
				</div><!-- justifize__box -->
				<div class="justifize__box">
					<?php get_template_part( 'partials/content/share' ); ?>
				</div><!-- justifize__box -->
			</div><!-- justifize mt+ -->

			<?php
			
				if ( in_category( 'blog' ) ) :
			
					/**
					 * Get post author meta
					 */
					get_template_part( 'partials/meta/post-author' );

					/**
					 * Get the comments list and form
					 * @link https://developer.wordpress.org/reference/functions/comments_template/
					 */
					comments_template( '', true );
					
				endif;
			?>

		</div><!-- story-main -->
	</div><!-- container -->
</article><!-- #post -->