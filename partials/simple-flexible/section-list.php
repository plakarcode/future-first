<?php
/**
 * List
 *
 * Template part for rendering ACF flexible sections - list
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
if ( have_rows( 'items' ) ) : ?>

<div class="container">
	<ul class="story-main">
		<?php while ( have_rows( 'items' ) ) : the_row( 'items' ); ?>
			<li><?php the_sub_field( 'item' ); ?></li>
		<?php endwhile; ?>
	</ul>
</div><!-- container -->

<?php endif;
