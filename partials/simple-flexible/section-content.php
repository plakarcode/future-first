<?php
/**
 * Regular content
 *
 * Template part for rendering ACF flexible sections - regular content
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
if ( ! get_sub_field( 'regular_content' ) ) {
	return;
} ?>

<div class="container">
	<div class="story-main">
		<?php the_sub_field( 'regular_content' ); ?>
	</div><!-- story-main -->
</div><!-- container -->
