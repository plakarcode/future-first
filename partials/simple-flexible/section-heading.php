<?php
/**
 * Heading
 *
 * Template part for rendering ACF flexible sections - heading
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
if ( ! get_sub_field( 'heading' ) ) {
	return;
} ?>
<div class="container">
	<h3 class="mt+ story-main"><?php the_sub_field( 'heading' ); ?></h3>
</div><!-- container -->
