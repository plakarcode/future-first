<?php
/**
 * Image
 *
 * Template part for rendering ACF flexible sections - image
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
if ( ! get_sub_field( 'image' ) ) {
	return;
}

	$image = get_sub_field( 'image' );
	/**
	 * Prepare sizes
	 * @var string
	 */
	$thumbnail = $image['sizes']['thumbnail'];
	$medium    = $image['sizes']['medium'];
	$large     = $image['sizes']['large'];
	$full      = $image['url'];
?>

<div class="container pb+">
	<img src="<?php echo $full; ?>" >
</div><!-- container -->
