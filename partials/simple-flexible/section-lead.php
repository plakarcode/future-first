<?php
/**
 * Leading paragraph
 *
 * Template part for rendering ACF flexible sections - leading paragraph
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
if ( ! get_sub_field( 'lead_paragraph' ) ) {
	return;
} ?>

<div class="container">
	<p class="intro-text story-main"><?php the_sub_field( 'lead_paragraph' ); ?></p>
</div><!-- container -->
