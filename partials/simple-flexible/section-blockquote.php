<?php
/**
 * Blockquote
 *
 * Template part for rendering ACF flexible sections - blockquote
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
if ( have_rows( 'blockquote' ) ) : while ( have_rows( 'blockquote' ) ) : the_row( 'blockquote' );

	if ( get_sub_field( 'quote' ) ) : ?>

		<div class="container mt">
			<blockquote class="quote">
				<p class="lead"><?php the_sub_field( 'quote' ); ?></p>
				<div class="subtitle__before"></div>
				<?php if ( get_sub_field( 'cite' ) ) : ?>
					<p class="subtitle">
						<cite><?php the_sub_field( 'cite' ); ?></cite>
					</p>
				<?php endif; // get_sub_field( 'cite' ) ?>
			</blockquote>
		</div><!-- container text-center mt+ -->

	<?php endif; // get_sub_field( 'quote' )

endwhile; endif;
