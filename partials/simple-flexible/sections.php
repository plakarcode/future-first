<?php
/**
 * Simple flexible sections
 *
 * Template part for rendering ACF simple flexible sections. Mostly used
 * on single posts.
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
while ( the_flexible_field( 'simple_content_fields' ) ) :

	// START HEADING SECTION
	if ( get_row_layout() == 'heading_layout' ) :
		get_template_part( 'partials/simple-flexible/section', 'heading' );
	// END HEADING SECTION

	// START LEAD PARAGRAPH SECTION
	elseif ( get_row_layout() == 'lead_layout' ) :
		get_template_part( 'partials/simple-flexible/section', 'lead' );
	// END LEAD PARAGRAPH SECTION

	// START REGULAR PARAGRAPH SECTION
	elseif ( get_row_layout() == 'content_layout' ) :
		get_template_part( 'partials/simple-flexible/section', 'content' );
	// END REGULAR PARAGRAPH SECTION

	// START LIST SECTION
	elseif ( get_row_layout() == 'list_layout' ) :
		get_template_part( 'partials/simple-flexible/section', 'list' );
	// END LIST SECTION

	// START BLOCKQUOTE SECTION
	elseif ( get_row_layout() == 'blockquote_layout' ) :
		get_template_part( 'partials/simple-flexible/section', 'blockquote' );
	// END BLOCKQUOTE SECTION

	// START HIGHLIGHT SECTION
	elseif ( get_row_layout() == 'highlight_layout' ) :
		get_template_part( 'partials/simple-flexible/section', 'highlight' );
	// END HIGHLIGHT SECTION

	// START IMAGE SECTION
	elseif ( get_row_layout() == 'image_layout' ) :
		get_template_part( 'partials/simple-flexible/section', 'image' );
	// END IMAGE SECTION

	// START HORIZONTAL RULE SECTION
	elseif ( get_row_layout() == 'image_with_text_layout' ) :
		get_template_part( 'partials/simple-flexible/section', 'image-with-text' );
	// END HORIZONTAL RULE SECTION

	// START GALLERY SECTION
	elseif ( get_row_layout() == 'gallery_layout' ) :
		get_template_part( 'partials/simple-flexible/section', 'gallery' );
	// END GALLERY SECTION

	// START IMAGE SLIDER SECTION
	elseif ( get_row_layout() == 'image_slider_section' ) :
		get_template_part( 'partials/simple-flexible/section', 'image-slider' );
	// END IMAGE SLIDER SECTION

	// START HORIZONTAL RULE SECTION
	elseif ( get_row_layout() == 'hr_layout' ) :
		get_template_part( 'partials/simple-flexible/section', 'hr' );
	// END HORIZONTAL RULE SECTION

	endif; // get_row_layout()

endwhile; // the_flexible_field( 'content_fields' )