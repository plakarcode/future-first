<?php
/**
 * Horizontal rule
 *
 * Template part for rendering ACF flexible sections - hr
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
$image = get_sub_field( 'image' ); ?>

<div class="container">
	<div class="story-main mt+ pb">
		<?php if ( $image ) : ?>
			<div class="wp-caption alignleft">
				<img src="<?php echo $image['sizes']['medium_large']; ?>">
				<?php if ( get_sub_field( 'image_caption' ) ) {
					the_sub_field( 'image_caption' );
				} ?>
			</div>
		<?php endif; // $image
		if ( get_sub_field( 'text' ) ) {
			the_sub_field( 'text' );
		} ?>
	</div>
</div><!-- container -->
