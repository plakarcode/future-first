<?php
/**
 * Template part for image slider flexible section
 *
 * @package WordPress
 */

/**
 * Fire all modification filters right before the silder,
 * so that we don't effect other sliders.
 *
 * @see inc/plugins/slider-filters.php
 */
add_filter( 'simple_slider_id', 'change_post_slider_id' );
add_filter( 'simple_slider_item_class', 'change_post_slider_item_class' );
add_filter( 'simple_slider_img', 'add_post_slider_img_caption' );

/**
 * Call the slider
 */
?>
<div class="container">
	<div class="story-main">
		<?php house_slider_simple( 'image_slider' ); ?>
	</div><!-- story-main -->
</div><!-- container -->
<?php
/**
 * To avoid this same filter apply to all other slider
 * instances, we are removing it right after calling the slider.
 *
 * @see inc/plugins/slider-filters.php
 */
remove_filter( 'simple_slider_id', 'change_post_slider_id' );
remove_filter( 'simple_slider_item_class', 'change_post_slider_item_class' );
remove_filter( 'simple_slider_img', 'add_post_slider_img_caption' );
