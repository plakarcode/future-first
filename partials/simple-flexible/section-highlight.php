<?php
/**
 * Blockquote
 *
 * Template part for rendering ACF flexible sections - blockquote
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */

if ( get_sub_field( 'highlight' ) ) : ?>

	<div class="container text-center mt+">
		<p class="highlight"><?php the_sub_field( 'highlight' ); ?></p>
	</div><!-- container text-center mt+ -->

<?php endif; // get_sub_field( 'highlight' )

