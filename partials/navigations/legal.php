<?php
/**
 * Legal navigation template part
 *
 * Template part for rendering legal navigation.
 *
 * @link https://codex.wordpress.org/Function_Reference/wp_nav_menu
 *
 * @package WordPress
 */
?>
<nav id="site-subnavigation" class="nav-secondary" role="navigation">
	<?php wp_nav_menu( [ 'theme_location' => 'legal', 'menu_class' => 'menu', 'container' => false ] ); ?>
</nav><!-- #site-subnavigation -->