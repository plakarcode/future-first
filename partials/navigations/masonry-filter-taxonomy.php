<?php
/**
 * Insights taxonomy filters template part
 *
 * Template part for rendering taxonomies filters for insights posts - blog.
 *
 * @package WordPress
 */
/**
 * Get category terms, hide empty and order by number of posts
 * @var array
 */
$terms = get_terms( array(
	'taxonomy'   => 'category',
	'orderby'    => 'count',
	'order'      => 'DESC',
	'hide_empty' => true,
));
/**
 * If on category archive, display category name as selected one
 */
if ( is_category() ) {
	$selected = single_cat_title( '', false );
} else {
	$selected = 'Category';
}
?>

<form id="insights--filterselect">
	<div class="float-right change-view">

		<?php if ( $terms ) : ?>
		<select data-custom-class="select--category select--category--dark text-right <?php if ( ! is_category() ) { echo 'large-and-up-mr'; } ?>" data-placeholder="<?php echo $selected; ?>" id="select--category">
			<option value="">All</option>
			<?php foreach ( $terms as $term ) : ?>
				<option value=".category-<?php echo $term->slug; ?>"><?php echo ucwords( $term->name ); ?></option>
			<?php endforeach; // $terms as $term ?>
		</select>
		<?php endif; // $terms ?>

		<?php
			/**
			 * On category archive display only list view
			 */
			if ( ! is_category() ) : ?>
			<div class="change-event-wrap">
				<a href="" class="change-view__list"><?php echo house_svg_icon( 'filter-list' ); ?></a>
				<a href="" class="change-view__grid is-active"><?php echo house_svg_icon( 'filter-grid' ); ?></a>
			</div>
		<?php endif; // ! is_category() ?>

	</div><!-- float-right change-view -->
</form><!-- #insights--filterselect -->