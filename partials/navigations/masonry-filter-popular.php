<?php
/**
 * Masonry filter - popular
 *
 * Template part for rendering masonry posts filter by popularity.
 *
 * @package WordPress
 */
if ( is_home() ) : ?>
	<select data-custom-class="select--category hide-lg" data-placeholder='Filter by'>
		<option value=".all">MOST RECENT</option>
		<option value=".popular">MOST POPULAR</option>
	</select>
<?php endif; ?>

<div class="masonry-filtering__nav">
	<a href="javascript:;" class="btn is-checked" data-filter=".all">MOST RECENT</a>
	<a href="javascript:;" class="btn " data-filter=".popular">MOST POPULAR</a>
</div><!-- masonry-filtering__nav -->