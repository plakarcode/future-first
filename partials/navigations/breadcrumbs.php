<?php
/**
 * Breadcrumb template part
 *
 * Template part for rendering breadcrumbs.
 *
 * @package WordPress
 */
/**
 * Get the posts page id
 * That's the only way to get page title for blog page
 */
global $globalSite, $post;
$blog = $globalSite['blog']; ?>

<ul class="breadcrumbs">

	<li><a href="<?php echo $globalSite['home']; ?>">Home</a></li>

	<?php
		/**
		 * Children of page Info
		 */
		if ( is_child_page( 'about-us' ) && $post->post_parent > 0 ) : ?>

		<li><a href="/about-us/">About Us</a></li>

	<?php endif;

	/**
	 * For single post and category archive add blog page
	 */
	if ( is_single() || is_category() || is_search() ) :

		/**
		 * This part doesn't exist on 'page' post type
		 */
		/**
		 * Resource posts - even though these are excluded from search query
		 */
		if ( 'resource' === get_post_type() ) : ?>

			<li><a href="<?php echo get_permalink( get_page_by_path( 'resources' ) ); ?>"><?php echo highlight_custom_search( 'Resources' ); ?></a></li>
	<?php
		/**
		 * Resource posts - even though these are excluded from search query
		 */
		elseif ( 'press' === get_post_type() ) : ?>

			<li><a href="<?php echo get_permalink( get_page_by_path( 'about-us/press' ) ); ?>"><?php echo highlight_custom_search( 'Press' ); ?></a></li>

	<?php
		/**
		 * Regular posts - insights
		 */
		elseif ( 'post' === get_post_type() ) : ?>

			<li><a href="<?php the_permalink( $blog ); ?>"><?php echo get_the_title( $blog ); ?></a></li>

	<?php endif; // get_post_type()

	/**
	 * For single posts add one category
	 */
	elseif ( is_single() ) :

		/**
		 * Get the first category
		 * @var array
		 */
		$categories = get_the_category( get_the_ID() );

		if ( ! empty( $categories ) ) :

			$cat_link = get_category_link( $categories[0]->term_id ); ?>

			<li><a href="<?php echo esc_url( $cat_link ); ?>"><?php echo $categories[0]->name; ?></a></li>

		<?php endif; // ! empty( $categories )

	endif; // is_single() || is_category()


	/**
	 * Active item - page we are on
	 */

	/**
	 * Title for Blog page - Insights
	 * index.php
	 */
	if ( is_home() ) : ?>

		<li class="is-active"><?php echo get_the_title( $blog ); ?></li>

	<?php
		/**
		 * Category title
		 */
		elseif ( is_category() ) : ?>

		<li class="is-active"><?php single_cat_title(); ?></li>

	<?php
		/**
		 * Search results
		 */
		elseif ( is_search() ) :
			/**
			 * Get post type
			 * @var string
			 */
			$post_type = get_post_type();
			/**
			 * Only for insight posts and pages
			 * because researches and resources have popups instead of singles
			 */
			if ( in_array( $post_type, array( 'post', 'page' ) ) ) : ?>

				<li class="is-active"><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></li>

		<?php endif;

		/**
		 * Page or post title
		 */
		else : ?>

		<li class="is-active"><?php the_title(); ?></li>

	<?php endif; // is_home() ?>
</ul>
