<?php
/**
 * Sidebar navigation template part
 *
 * Template part for rendering sidebar navigation.
 *
 * @link https://codex.wordpress.org/Function_Reference/wp_nav_menu
 *
 * @package WordPress
 */
?>
<nav id="site-subnavigation" class="nav-secondary" role="navigation">
	<a href="javascript:;" class="nav-secondary__toggler"><?php if ( is_page( 'about-us' ) ) { echo "About Us"; } else { echo "Menu"; }; ?></a>
	<?php wp_nav_menu( [ 'theme_location' => 'sidebar', 'menu_class' => 'menu', 'container' => false ] ); ?>
</nav><!-- #site-subnavigation -->