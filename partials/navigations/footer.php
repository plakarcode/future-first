<?php
/**
 * Footer navigation template part
 *
 * Template part for rendering footer navigation.
 *
 * @link https://codex.wordpress.org/Function_Reference/wp_nav_menu
 *
 * @package WordPress
 */
?>
<nav id="site-subnavigation" class="footer-main__nav" role="navigation">
	<?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_class' => 'menu', 'container' => 'ul' ) ); ?>
</nav><!-- #site-subnavigation -->