<?php
/**
 * Resources filters template part
 *
 * Template part for rendering custom taxonomies filters for resources cpt.
 *
 * @package WordPress
 */
/**
 * Get type terms, hide empty and order by number of posts
 * @var array
 */
$terms = get_terms( array(
	'taxonomy'   => 'format',
	'orderby'    => 'count',
	'order'      => 'DESC',
	'hide_empty' => true,
));

$hide_filters = get_field('hide_filters');

if ( $terms && !$hide_filters ) : ?>
<form id="resources__filterselect" class="hide-lg">
	<fieldset>
		<select data-custom-class="select--category" data-placeholder='All'>
			<option value="">All</option>
			<?php foreach ( $terms as $term ) : ?>
				<option value=".category-<?php echo $term->slug; ?>"><?php echo ucwords( $term->name ); ?></option>
			<?php endforeach; // $terms as $term ?>
		</select>
	</fieldset>
</form>

<ul class="tabs__nav show-lg">
	<li><a class="filter" data-filter="all">All</a></li>
	<?php foreach ( $terms as $term ) : ?>
		<li><a class="filter" data-filter=".category-<?php echo $term->slug; ?>"><?php echo ucwords( $term->name ); ?></a></li>
	<?php endforeach; // $terms as $term ?>
</ul>
<?php endif; // $terms