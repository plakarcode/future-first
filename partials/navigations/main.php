<?php
/**
 * Main navigation template part
 *
 * Template part for rendering main navigation.
 *
 * @link https://codex.wordpress.org/Function_Reference/wp_nav_menu
 *
 * @package WordPress
 */
?>
<nav id="site-navigation" class="main-navigation nav-primary" role="navigation">
	<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => 'ul' ) ); ?>
	<ul id="menu-main-menu-x" class="menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-x"><a class="openpopup" href="#login-form">Login</a></li>
	</ul>
</nav><!-- #site-navigation -->