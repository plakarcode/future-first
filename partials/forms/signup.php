<?php
/**
 * Login
 *
 * Template part for rendering login popup
 *
 * @package  WordPress
 */
?>
<div id="signup-form" class="magnific-popup mfp-hide magnific-animate">
	<div class="login-signup-buttons">
		<a href="#login-form" class="openpopup 1/2">Login</a>
		<a href="javascript:;" class="1/2 is-active">Sign up</a>
	</div><!-- login-signup-buttons -->

	<div class="signup-form__content">
		<div class="layout popup-form">

			<div class="layout__item 1/2 text-center">
				<?php if ( have_rows( 'popup_signup_school', 'option' ) ) : while ( have_rows( 'popup_signup_school', 'option' ) ) : the_row(); ?>
					<div class="form-content-wrap">
						<div class="mb"><?php echo house_svg_icon( 'schools' ); ?></div>

						<?php if ( get_sub_field( 'title', 'option' ) ) : ?>
							<h3 class="mb"><?php the_sub_field( 'title', 'option' ); ?></h3>
						<?php endif; // get_sub_field( 'title', 'option' )

						if ( get_sub_field( 'intro', 'option' ) ) : ?>
							<p class="mb"><?php the_sub_field( 'intro', 'option' ); ?></p>
						<?php endif; // get_sub_field( 'intro', 'option' ) ?>

						<?php if ( get_sub_field( 'signup_url', 'option' ) ) : ?>
							<a class="btn btn--primary" href="<?php the_sub_field( 'signup_url', 'option' ); ?>" target="_blank">Sign Up</a>
						<?php endif; // get_sub_field( 'signup_url', 'option' ) ?>

					</div><!-- end of .form-content-wrap -->
				<?php endwhile; endif; // have_rows( 'popup_signup_school', 'option' ) ?>
			</div><!-- layout__item 1/2 text-center -->

			<div class="layout__item 1/2 text-center">
				<?php if ( have_rows( 'popup_signup_student', 'option' ) ) : while ( have_rows( 'popup_signup_student', 'option' ) ) : the_row(); ?>
					<div class="form-content-wrap">
						<div class="mb"><?php echo house_svg_icon( 'students' ); ?></div>

						<?php if ( get_sub_field( 'title', 'option' ) ) : ?>
							<h3 class="mb"><?php the_sub_field( 'title', 'option' ); ?></h3>
						<?php endif; // get_sub_field( 'title', 'option' )

						if ( get_sub_field( 'intro', 'option' ) ) : ?>
							<p class="mb"><?php the_sub_field( 'intro', 'option' ); ?></p>
						<?php endif; // get_sub_field( 'intro', 'option' ) ?>

						<?php if ( get_sub_field( 'signup_url', 'option' ) ) : ?>
							<a class="btn btn--primary" href="<?php the_sub_field( 'signup_url', 'option' ); ?>" target="_blank">Sign Up</a>
						<?php endif; // get_sub_field( 'signup_url', 'option' ) ?>

					</div><!-- end of .form-content-wrap -->
				<?php endwhile; endif; // have_rows( 'popup_signup_student', 'option' ) ?>
			</div><!-- layout__item 1/2 text-center -->

		</div><!-- layout popup-form -->
	</div><!-- signup-form__content -->

	<div class="mfp-close"><span>close</span> <?php echo house_svg_icon( 'close' ); ?></div>
</div><!-- magnific-popup mfp-hide magnific-animate -->
