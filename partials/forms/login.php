<?php
/**
 * Login
 *
 * Template part for rendering login popup
 *
 * @package  WordPress
 */
?>

<div id="login-form" class="magnific-popup mfp-hide magnific-animate">
	<div class="login-signup-buttons">
		<a href="javascript:;" class="1/2 is-active">Login</a>
		<a href="#signup-form" class="openpopup 1/2">Sign up</a>
	</div><!-- login-signup-buttons -->

	<form action="some-action" class="popup-form">
		<div class="form-content-wrap">

			<?php if ( have_rows( 'popup_login', 'option' ) ) : while ( have_rows( 'popup_login', 'option' ) ) : the_row(); ?>
				<header>
					<?php if ( get_sub_field( 'title', 'option' ) ) : ?>
						<h3><?php the_sub_field( 'title', 'option' ); ?></h3>
					<?php endif; // get_sub_field( 'title', 'option' )

					if ( get_sub_field( 'intro', 'option' ) ) :
						the_sub_field( 'intro', 'option' );
					endif; // get_sub_field( 'intro', 'option' ) ?>
				</header>

				<?php if ( get_sub_field( 'login_url', 'option' ) ) : ?>
					<a class="btn btn--primary" href="<?php the_sub_field( 'login_url', 'option' ); ?>" target="_blank">Login</a>
				<?php endif; // get_sub_field( 'login_url', 'option' ) ?>
			<?php endwhile; endif; // have_rows( 'popup_login', 'option' ) ?>

			<?php
				/**
				 * We are not going to log in anyone to WordPress
				 * and still don't have any API for logging anyone to other sites
				 */
			?>
			<!-- <input type="email" class="input input--primary mb- desktop-and-up-mb" placeholder="email address"> -->
			<!-- <input type="password" class="input input--primary mb- desktop-and-up-mb" placeholder="password"> -->
			<!-- <button class="btn btn--primary btn--full" type="submit">Login</button> -->

			<!-- <div class="popup-form__bottom-link">
				<a href="#forgot-password-form" class="openpopup">forgot password</a>
			</div> -->
			<!-- popup-form__bottom-link -->

		</div><!-- end of .form-content-wrap -->
	</form><!-- popup-form -->

	<div class="mfp-close"><span>close</span> <?php echo house_svg_icon( 'close' ); ?></div>
</div><!-- magnific-popup mfp-hide magnific-animate -->
