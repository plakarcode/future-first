<?php
/**
 * Contact Form
 *
 * The template for rendering Contact Form.
 *
 * @package WordPress
 * @subpackage Contact Form 7
 */
/**
 * Don't even bother if plugin is not active
 */
if ( ! house_is_plugin_active( 'contact-form-7/wp-contact-form-7.php' ) ) {
	return;
}
/**
 * Don't do anything if we have no form id
 */
if ( get_field( 'contact_form_id' ) ) : ?>

	<section class="send-us-msg">
		<div class="container">
			<div class="layout">
				<div class="layout__item extralarge-and-up-4/12">
					<?php
						/**
						 * Get section title
						 */
						if ( get_field( 'contact_section_title' ) ) : ?>

							<h2><?php the_field( 'contact_section_title' ); ?></h2>

						<?php endif; // get_field( 'contact_section_title' )

						/**
						 * Get section intro
						 */
						if ( get_field( 'contact_section_intro' ) ) : ?>

							<p><?php the_field( 'contact_section_intro' ); ?></p>

						<?php endif; // get_field( 'contact_section_intro' )
					?>
				</div><!-- layout__item extralarge-and-up-4/12 -->

				<div class="layout__item extralarge-and-up-8/12">
					<?php
						/**
						 * Print the form
						 */
						echo do_shortcode( '[contact-form-7 id="' . get_field( 'contact_form_id' ) . '"]' );
					?>
				</div><!-- layout__item extralarge-and-up-8/12 -->
			</div><!-- layout -->
		</div><!-- container -->
	</section><!-- send-us-msg -->

<?php endif; // get_field( 'contact_form_id' )