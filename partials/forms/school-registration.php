<?php
/**
 * Contact Form
 *
 * The template for rendering Contact Form.
 *
 * @package WordPress
 * @subpackage Contact Form 7
 */
/**
 * Don't even bother if plugin is not active
 */
if ( ! house_is_plugin_active( 'contact-form-7/wp-contact-form-7.php' ) ) {
	return;
}
/**
 * Don't do anything if we have no form id
 */
if ( get_field( 'registration_form_id' ) ) : ?>

	<div class="school-registration__form">
		<div class="container">
			<div class="mb+ text-center"><?php echo house_svg_icon( 'schools' ); ?></div>
			<?php echo do_shortcode( '[contact-form-7 id="' . get_field( 'registration_form_id' ) . '"]' ); ?>
		</div><!-- container-->
	</div><!-- school-registration__form -->

<?php endif; //  get_field( 'registration_form_id' )