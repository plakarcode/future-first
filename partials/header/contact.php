<?php
/**
 * Contact
 *
 * Template part for rendering contact info in header
 *
 * @package  WordPress
 */

/**
 * Get the email address
 */
if ( get_field( 'email_address', 'option' ) ) : ?>
	<a href="mailto:<?php the_field( 'email_address', 'option' ); ?>" class="header-main__email"><?php the_field( 'email_address', 'option' ); ?></a>
<?php endif;

/**
 * Get the phone number
 */
if ( get_field( 'phone_number', 'option' ) ) : ?>
	<span class="vertical-line"></span>
	<a href="tel:<?php the_field( 'phone_number', 'option' ); ?>"><?php the_field( 'phone_number', 'option' ); ?></a>
<?php endif; ?>

<span class="vertical-line"></span>
<a href="#login-form" class="openpopup header-main__top--login type-uppercase">Login</a>
