<?php
/**
 * Social
 *
 * Template part for rendering social links in header
 *
 * @package  WordPress
 */
?>
<ul class="header-main__social">
	<?php
	if ( get_field( 'social_network_profiles', 'option' ) ) :
		$profiles = get_field( 'social_network_profiles', 'option' );

		foreach ( $profiles as $profile ) :
			if ( $profile['url'] ) :
				$link = '<a href="' . $profile['url'] . '" target="_blank">';
				$link .= ucwords( $profile['network'] );
				$link .= '</a>';

				echo '<li>' . $link . '</li>';

			endif; // $profile['url']
		endforeach; // $profiles as $profile
	endif; // get_field( 'social_network_profiles', 'option' )  ?>
</ul>
