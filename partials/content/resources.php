<?php
/**
 * Resources
 *
 * Template part for rendering resources tabs.
 *
 * @package WordPress
 */
$sources = get_resources();

if ( $sources->have_posts() ) : ?>

<div class="tabs tabs--magic mb filter-menu masonry-filtering resources__dropdown">

	<?php
		/**
		 * Get taxonomy filter for resources
		 */
		get_template_part( 'partials/navigations/filters', 'resources' ); ?>

	<div id="general-filter" class="container-filter">
		<div class="layout">

		<?php while ( $sources->have_posts() ) : $sources->the_post();

			if ( post_type_exists( get_post_type() ) ) {
				get_template_part( 'content', get_post_type() );
			}
			else {
				get_template_part( 'content' );
			}

		endwhile; // $sources->have_posts() ?>

		</div><!-- layout -->
	</div><!-- container-filter -->
</div><!-- tabs tabs--magic mb filter-menu masonry-filtering resources__dropdown -->

<div class="text-center">
	<?php house_simple_content_pagination( 'pagination', $sources, 'pagination' ); ?>
</div><!-- text-center -->
<?php endif; // $sources->have_posts()