<?php
/**
 * Home Story
 *
 * Template part for rendering story section on home page.
 *
 * @package WordPress
 */

$link_to_story = get_field( 'link_to_story' );
?>
<section class="story">
	<div class="container text-center">

		<?php if ( $link_to_story ) : ?>
			<a href="<?php the_field( 'story_case_study' ); ?>">
		<?php endif; ?>
		
			<div class="story__thumbnail <?php if ( !$link_to_story ) : ?>js-playvideo<?php endif; ?>">
				<?php
					/**
					 * Get image
					 */
					if ( get_field( 'story_image' ) ) : ?>
						<img src="<?php the_field( 'story_image' ); ?>" alt="Story" />
					<?php endif; // get_field( 'story_image' ) ?>


				<div class="story__content">
					<?php

						/**
						 * Get story title
						 */
						if ( get_field( 'story_title' ) ) :	?>

							<h2><?php the_field( 'story_title' ); ?></h2>

						<?php endif; // get_field( 'story_title' )

						/**
						 * Get intro
						 */
						if ( get_field( 'story_intro' ) ) : ?>

							<p><?php the_field( 'story_intro' ); ?></p>

						<?php endif; // get_field( 'story_intro' )

						if ( $link_to_story ) :
							
							echo "<p>READ MORE</p>";
							
						else:

							/**
							 * Get icon
							 */
							echo house_svg_icon( 'video-play', 'icon-play' ); ?>
							<p>PLAY VIDEO</p>

					<?php endif; ?>

				</div><!-- end of .story__content -->
			</div><!-- story__thumbnail js-playvideo -->
			
		<?php if ( $link_to_story ) : ?>
			</a>
		<?php endif; ?>

		<?php
			/**
			 * Get video
			 */
			if ( get_field( 'story_video' ) && !$link_to_story ) : ?>

				<div class="story__video">
					<div class="video-wrapper embedded embedded--16by9">
						<?php video_iframe( get_field( 'story_video' ) ); ?>
					</div> <!-- video-wrapper -->
				</div> <!-- story__video -->

			<?php endif; // get_field( 'story_video' )
		?>


	</div><!-- end of .container -->
</section><!-- end of .story -->
