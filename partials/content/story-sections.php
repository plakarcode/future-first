<?php
/**
 * Story flexible sections
 *
 * Template part for rendering ACF flexible sections for story cpt.
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
while ( the_flexible_field( 'story_popup_content' ) ) :

	// START HEADING SECTION
	if ( get_row_layout() == 'heading_layout' ) :
		if ( get_sub_field( 'heading' ) ) : ?>
			<h3><?php the_sub_field( 'heading' ); ?></h3>
		<?php endif; // get_sub_field( 'heading' )
	// END HEADING SECTION

	// START REGULAR PARAGRAPH SECTION
	elseif ( get_row_layout() == 'paragraph_layout' ) :
		if ( get_sub_field( 'paragraph' ) ) :
			the_sub_field( 'paragraph' );
		endif; // get_sub_field( 'paragraph' )
	// END REGULAR PARAGRAPH SECTION

	// START BLOCKQUOTE SECTION
	elseif ( get_row_layout() == 'blockquote_layout' ) :
		if ( have_rows( 'blockquote' ) ) : while ( have_rows( 'blockquote' ) ) : the_row( 'blockquote' );
			if ( get_sub_field( 'quote' ) ) : ?>
				<blockquote class="quote">
					<p class="lead"><?php the_sub_field( 'quote' ); ?></p>
					<div class="subtitle__before"></div>
					<?php if ( get_sub_field( 'cite' ) ) : ?>
						<p class="subtitle">
							<cite><?php the_sub_field( 'cite' ); ?></cite>
						</p>
					<?php endif; // get_sub_field( 'cite' ) ?>
				</blockquote>

			<?php endif; // get_sub_field( 'quote' )
		endwhile; endif; // have_rows( 'blockquote' )
	// END BLOCKQUOTE SECTION

	endif; // get_row_layout()

endwhile; // the_flexible_field( 'story_popup_content' )