<?php
/**
 * Supporters
 *
 * Template part for rendering supporters tab.
 *
 * @package WordPress
 */
if ( have_rows( 'supporters' ) ) : ?>

	<div class="layout layout--center layout--middle">

		<?php while ( have_rows( 'supporters' ) ) : the_row();
			/**
			 * Get logo
			 * @var string
			 */
			$image = get_sub_field( 'logo' );
			/**
			 * Get website url and build link
			 * @var string
			 */
			$link = get_sub_field( 'website' ); ?>

			<div class="layout__item extralarge-and-up-1/4 large-and-up-1/3 medium-and-up-1/2">
				<?php if ( $image ) : ?>
					<div class="employers__grid--logo-box">
						<a class="grid-item__logo" href="<?php echo $link; ?>" target="_blank">
							<img src="<?php echo $image; ?>" >
						</a>
					</div><!-- employers__grid--logo-box -->
				<?php endif; // $image

				if ( get_sub_field( 'name' ) ) : ?>
					<p><?php the_sub_field( 'name' ); ?></p>
				<?php endif; //  get_sub_field( 'name' ) ?>

				<?php if ( $link ) : ?>
					<a class="employers__grid--read-more" href="<?php echo $link; ?>">READ MORE</a>
				<?php endif; // $link ?>
			</div><!-- layout__item extralarge-and-up-1/4 large-and-up-1/3 medium-and-up-1/2 -->

		<?php endwhile; // have_rows( 'supporters' ) ?>

	</div><!-- layout layout--center layout--middle -->

<?php endif; // have_rows( 'supporters' )