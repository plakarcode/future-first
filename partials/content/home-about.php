<?php
/**
 * Home About
 *
 * Template part for rendering about section on home page.
 *
 * @package WordPress
 */
?>
<section class="find-more">
	<div class="container">
		<header>
			<?php
				/**
				 * Get section title
				 */
				if ( get_field( 'about_section_title' ) ) : ?>

					<h2><?php the_field( 'about_section_title' ); ?></h2>

				<?php endif; // get_field( 'about_section_title' )
				/**
				 * Ge tsection intro
				 */
				if ( get_field( 'about_section_intro' ) ) : ?>

					<p><?php the_field( 'about_section_intro' ); ?></p>

				<?php endif; // get_field( 'about_section_intro' )
			?>
		</header>

		<?php if ( have_rows( 'about_section_items' ) ) : ?>
		<div class="layout layout--center">

			<?php while ( have_rows( 'about_section_items' ) ) : the_row(); ?>
			<div class="layout__item large-and-up-1/3">
				<div class="find-more__item">
				<?php
					/**
					 * Get icon
					 */
					if ( get_sub_field( 'select_item' ) ) :
						echo house_svg_icon( get_sub_field( 'select_item' ) . '-2' );
					endif; // get_sub_field( 'select_item' )

					if ( get_sub_field( 'number' ) ) : ?>
						<div class="find-more__item-number count">
							<?php the_sub_field( 'number' ); ?>
						</div>
					<?php endif; //  get_sub_field( 'number' )

					if ( get_sub_field( 'tagline' ) ) : ?>
						<p><?php the_sub_field( 'tagline' ); ?></p>
					<?php endif; // get_sub_field( 'tagline' ) ?>
				</div><!-- end of .find-more__item -->
			</div><!-- end of .layout__item -->
			<?php endwhile; // have_rows( 'about_section_items' ) ?>

		</div><!-- end of .layout -->
		<?php endif; // have_rows( 'about_section_items' ) ?>

	</div><!-- end of .container -->
</section><!-- end of .find-more -->