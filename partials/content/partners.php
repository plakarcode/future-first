<?php
/**
 * Partners
 *
 * Template part for rendering partners section.
 *
 * @package WordPress
 */
?>
<section class="partners">
	<div class="container">
		<header>
			<?php if ( get_field( 'partners_section_title' ) ) : ?>
				<h2><?php the_field( 'partners_section_title' ); ?></h2>
			<?php endif; // get_field( 'partners_section_title' )

			if ( get_field( 'partners_section_intro' ) ) : ?>
				<p><?php the_field( 'partners_section_intro' ); ?></p>
			<?php endif; // get_field( 'partners_section_intro' ) ?>
		</header>

		<?php if ( have_rows( 'partners_home' ) ) :
			$count = 1; ?>
		<div class="layout layout--center layout--middle">

			<?php while ( have_rows( 'partners_home' ) ) : the_row();

				/**
				 * Make id from name
				 * @var string
				 */
				if ( get_sub_field( 'name' ) ) {
					$id = sanitize_title_with_dashes( get_sub_field( 'name' ) );
				} else {
					$id = $count++;
				}
				/**
				 * Get logo
				 * @var string
				 */
				$image = get_sub_field( 'logo' ); ?>

				<div class="layout__item extralarge-and-up-1/4 large-and-up-1/3 medium-and-up-1/2">
					<?php if ( $image ) : ?>
						<div class="employers__grid--logo-box">
							<a class="openpopup grid-item__logo" href="#employers-<?php echo $id; ?>">
								<img src="<?php echo $image; ?>" >
							</a>
						</div><!-- employers__grid--logo-box -->
					<?php endif; // $image ?>
				</div><!-- layout__item extralarge-and-up-1/4 large-and-up-1/3 medium-and-up-1/2 -->

			<?php endwhile; // have_rows( 'partners_home' ) ?>
		</div><!-- layout layout--center layout--middle -->

		<?php
			$count = 1;

			while ( have_rows( 'partners_home' ) ) : the_row();
			/**
			 * Make id from name
			 * @var string
			 */
			if ( get_sub_field( 'name' ) ) {
				$id = sanitize_title_with_dashes( get_sub_field( 'name' ) );
			} else {
				$id = $count++;
			}
			/**
			 * Get logo
			 * @var string
			 */
			$image = get_sub_field( 'logo' );
			/**
			 * Get website url and build link
			 * @var string
			 */
			$link = get_sub_field( 'website' );
			if ( $link ) {
				$parse = parse_url( $link );
				$website = $parse['host'];
			} ?>

			<div class="employers-lightbox magnific-popup mfp-hide magnific-animate" id="employers-<?php echo $id; ?>">
				<div class="employers-lightbox__content pt+ ph">
					<div class="layout layout--middle">
						<div class="layout__item large-and-up-5/12 employers-lightbox__content-title text-center">
							<?php if ( $image ) : ?>
								<img src="<?php echo $image; ?>" >
							<?php endif; // $image ?>

							<?php if ( get_sub_field( 'name' ) ) : ?>
								<h3 class="h2"><?php the_sub_field( 'name' ); ?></h3>
							<?php endif; //  get_sub_field( 'name' ) ?>

							<hr class="break-small">
							<?php if ( $link ) : ?>
								<a class="employers-lightbox__website-link" href="<?php echo $link; ?>" target="_blank"><?php echo $website; ?></a>
							<?php endif; // $link ?>
						</div>

						<div class="layout__item large-and-up-7/12 employers-lightbox__content-main">
							<?php if ( get_sub_field( 'description' ) ) : ?>
								<?php the_sub_field( 'description' ); ?>
							<?php endif; //  get_sub_field( 'description' ) ?>
						</div>
					</div>
				</div>
				<div class="mfp-close">
					<span>close</span> <?php echo house_svg_icon( 'close' ); ?>
				</div>
			</div><!-- employers-lightbox magnific-popup mfp-hide magnific-animate -->

		<?php endwhile; // have_rows( 'partners_home' ) ?>
	<?php endif; // have_rows( 'partners_home' ) ?>

	<?php if ( is_front_page() ) : ?>
		<?php if ( get_field( 'partners_section_button_link' ) ) : ?>
			<div class="mt">
				<a href="<?php the_field( 'partners_section_button_link' ); ?>" class="btn btn--primary">View all</a>
			</div>
		<?php endif; // get_field( 'partners_section_button_link' ) ?>
	<?php endif; // is_front_page() ?>
	</div><!-- end of .container -->
</section><!-- end of .partners -->