<?php
/**
 * News
 *
 * Template part for rendering news section.
 *
 * @package WordPress
 */
?>
<section class="news pt+ pb++">
	<div class="container text-center">
		<?php
			/**
			 * Get section title
			 */
			if ( get_field( 'news_section_title' ) ) : ?>
				<h2><?php the_field( 'news_section_title' ); ?></h2>
			<?php endif; // get_field( 'news_section_title' )

			/**
			 * Get intro
			 */
			if ( get_field( 'news_section_intro' ) ) : ?>
				<p><?php the_field( 'news_section_intro' ); ?></p>
			<?php endif; // get_field( 'news_section_intro' )
		?>
	</div><!-- end of .container -->

	<div class="container">
		<?php get_template_part( 'partials/content/news', 'masonry' ); ?>
	</div>
</section><!-- end of .news -->