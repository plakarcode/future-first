<?php
/**
 * News masonry
 *
 * Template part for rendering news masonry section.
 *
 * @package WordPress
 */
global $wp_query;
/**
 * Set class for news and category page
 */
if ( is_home() || is_category() ) {
	$class = 'container-filter';
} else {
	$class = '';
}
/**
 * Set default view
 */
if ( is_category() ) {
	$view = 'list';
} else {
	$view = 'grid';
}
?>

<div class="masonry-filtering masonry-filtering--<?php echo $view; ?>">

	<?php
		/**
		 * Home page
		 */
		 if ( is_front_page() ) : ?>

		<div class="justifize justifize--bottom">
			<div class="justifize__box">
				<?php // get_template_part( 'partials/navigations/masonry-filter', 'popular' ); ?>
			</div><!-- justifize__box -->
		</div><!-- justifize justifize--bottom -->

	<?php
		/**
		 * Posts page 'index.php'
		 */
		else : ?>

		<div class="justifize justifize--middle">
			<div class="justifize__box text-left-sm">
				<?php // get_template_part( 'partials/navigations/masonry-filter', 'popular' ); ?>
			</div><!-- justifize__box -->

			<div class="justifize__box">
				<?php get_template_part( 'partials/navigations/masonry-filter', 'taxonomy' ); ?>
			</div><!-- justifize__box -->
		</div><!-- justifize justifize--bottom -->

	<?php endif; // is_front_page() ?>

<?php
	/**
	 * Home and News pages query
	 */
	if ( is_front_page() || is_home() ) :
		/**
		 * Fire filter for modifying query
		 * @see get_ajax_query_args()
		 * @see inc/content/ajax/post-queries.php
		 */
		add_filter( 'buzz_posts_query', 'get_ajax_query_args' );
		/**
		 * Get the query
		 * @var obj
		 */
		if ( function_exists( 'get_all_posts_query' ) ) {
			$query = get_all_posts_query( 'twitter_data_wrap', 'option' );
		} else {
			$query = $wp_query;
		}

		if ( $query->have_posts() ) : ?>

			<div id="insights-filter" class="masonry-filtering__content <?php echo $class; ?>">
				<div class="grid-sizer"></div>
				<div class="gutter-sizer"></div>

				<?php while ( $query->have_posts() ) : $query->the_post();

					get_template_part( 'content', 'masonry' );

				endwhile; // $query->have_posts() ?>

			</div><!-- masonry-filtering__content <?php echo $class; ?> -->

		<?php endif; // $query->have_posts()

		/**
		 * Get 'posts_per_page' setting
		 * @var int
		 */
		$posts_per_page = $query->query['posts_per_page'];
		
		/**
		 * Get total number of posts found by query
		 * @var int
		 */
		$found_posts = $query->found_posts;
		/**
		 * Show load more button only if
		 * number of found posts if larger than 'posts_per_page'.
		 * We don't want button that does nothin'.
		 */
		if ( $found_posts > $posts_per_page ) : ?>
			<div class="text-center mt+">
				<button id="more_posts" class="btn btn--primary">Load More</button>
			</div><!-- text-center mt+ -->
		<?php endif; //$found_posts > $posts_per_page

		/**
		 * Remove filter for modifying query
		 * so that it doesn't effect other queries
		 *
		 * @see get_ajax_query_args()
		 * @see inc/content/ajax/post-queries.php
		 */
		remove_filter( 'buzz_posts_query', 'get_ajax_query_args' );
		/**
		 * Always reset custom queries
		 */
		wp_reset_postdata();

	/**
	 * Category archive query
	 */
	elseif ( is_category() ) :
		/**
		 * Back to WordPress default query
		 */
		global $wp_query;

		if ( have_posts() ) : ?>
			<div id="insights-filter" class="masonry-filtering__content <?php echo $class; ?>">
				<div class="grid-sizer"></div>
				<div class="gutter-sizer"></div>

				<?php while ( have_posts() ) : the_post();
					get_template_part( 'content', 'masonry' );
				endwhile; ?>
			</div><!-- masonry-filtering__content -->

			<div class="text-center mt">
				<?php house_simple_content_pagination( 'pagination', $wp_query, 'pagination' ); ?>
			</div><!-- text-center -->

		<?php endif; // have_posts()

	endif; // is_front_page() || is_home() ?>

</div><!-- masonry-filtering masonry-filtering--<?php echo $view; ?> -->