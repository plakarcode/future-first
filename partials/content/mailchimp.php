<?php
/**
 * MailChimp
 *
 * Template part for rendering mailchimp form
 *
 * @package WordPress
 */
/**
 * Don't even bother if we have no API Key or list ID
 */
if ( ! get_field( 'mailchimp_api_key', 'option' ) || ! get_field( 'mailchimp_list_id', 'option' ) ) {
	return;
}
/**
 * API Key
 * Account -> Extras -> API Keys
 *
 * @var MailChimp
 */
if ( get_field( 'mailchimp_api_key', 'option' ) ) {
	$apikey = get_field( 'mailchimp_api_key', 'option' );
} else {
	$apikey = '';
}
/**
 * List ID
 * @var string
 */
if ( get_field( 'mailchimp_list_id', 'option' ) ) {
	$list_id = get_field( 'mailchimp_list_id', 'option' );
} else {
	$list_id = '';
}
/**
 * Check if email address is submitted
 * and set $email var
 */
if ( ! empty( $_POST['email'] ) ) {
	$email = $_POST['email'];
} else {
	$email = '';
}
/**
 * Define vars
 * @var string
 */
$message = '';
$class = '';
$icon = '';
/**
 * If email is submitted add subscriber to the list
 * and build the thank you message
 */
if ( isset( $_POST['email'] ) && ! empty( $_POST['email'] ) ) {
	/**
	 * Get the MailChimp response
	 * @var array
	 */
	$response = get_mailchimp_response( $list_id, $email, $apikey );

	/**
	 * If status is not 'subscribed' we have error
	 */
	if ( $response['status'] !== 'subscribed' ) {
		$message = 'Error: ' . $response['title'] . '.';
		$class = 'error1';
		$icon = house_svg_icon( 'exclamation' );

	} else {

		if ( get_field( 'mailchimp_thank_you_message', 'option' ) ) {
			$message = get_field( 'mailchimp_thank_you_message', 'option' );
		} else {
			$message = __( 'Thank you for subscribing.', 'house' );
		}

		$class = 'thanks';
		$icon = house_svg_icon( 'like', 'icon-like-small' );
	}
}
/**
 * If empty email field is submitted
 */
elseif ( isset( $_POST['email'] ) && empty( $_POST['email'] ) ) {

	if ( get_field( 'mailchimp_empty_field_message', 'option' ) ) {
		$message = get_field( 'mailchimp_empty_field_message', 'option' );
	} else {
		$message = __( 'Address field is empty. Please type your email address and try submitting again.', 'house' );
	}

	$class = 'error2';
	$icon = house_svg_icon( 'info2' );
}
?>


<form action="#subscribe" class="subscribe__form" method="POST">
	<?php if ( isset( $_POST['email'] ) && ! empty( $_POST['email'] ) ) : ?>
	<input type="email" name="email" id="email" class="input input--primary" placeholder="<?php echo $_POST['email']; ?>">
	<?php else : ?>
	<input type="email" name="email" id="email" class="input input--primary" placeholder='EMAIL ADDRESS'>
	<?php endif; // isset( $_POST['email'] ) && ! empty( $_POST['email'] ) ?>
	<input type="hidden" value="submit" name="submit" id="submit" class="btn btn--icon">
	<button class="btn btn--icon"><?php echo house_svg_icon( 'arrow-right' ); ?></button>
</form>

<div class="subscribe__social">
	<?php
		/**
		 * Get social title
		 */
		if ( get_field( 'subscribe_social_networks_title', 'option' ) ) : ?>
			<h3 class="h4"><?php the_field( 'subscribe_social_networks_title', 'option' ); ?></h3>
		<?php endif; // get_field( 'subscribe_social_networks_title', 'option' )
	?>
	<ul>
	<?php
		/**
		 * Get social links
		 */
		if ( get_field( 'social_network_profiles', 'option' ) ) :
			$profiles = get_field( 'social_network_profiles', 'option' );

			foreach ( $profiles as $profile ) :
				if ( $profile['url'] ) :
					$link = '<a href="' . $profile['url'] . '" target="_blank">';
					$link .= house_svg_icon( $profile['network'] );
					$link .= '</a>';

					echo '<li>' . $link . '</li>';

				endif; // $profile['url']
			endforeach; // $profiles as $profile
		endif; // get_field( 'social_network_profiles', 'option' )  ?>
	</ul>
</div><!-- subscribe__social -->

<?php if ( isset( $_POST['email'] ) ) : ?>
<div class="subscribe-message-wrap">
	<div class="subscribe-message subscribe-message--<?php echo $class; ?>">
		<span class="subscribe-message__icon"><?php echo $icon; ?></span>
		<span class="subscribe-message__text"><?php echo $message; ?></span>
		<span class="subscribe-message__close"><a href=""><?php echo house_svg_icon( 'close' ); ?></a></span>
	</div><!-- end of .subscribe-message -->
</div><!-- end of .subscribe-message-wrap -->
<?php endif; // isset( $_POST['email'] ) ?>