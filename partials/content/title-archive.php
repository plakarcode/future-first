<?php
/**
 * Archives titles
 *
 * Template part for rendering titles on non-singular entries (archives, search, 404..)
 *
 * @package WordPress
 */
if ( is_singular() || is_front_page() )
	return;

global $globalSite, $wp_locale; ?>

<div class="entry-hgroup">
	<?php
	// error page - 404.php
	if ( is_404() ) { ?>
		<h1 class="entry-title"><?php _e( 'This page doesn\'t exist', 'house' ); ?></h1>

		<div class="description">
			<?php echo wpautop( __( 'It seems we can\'t find what you\'re looking for.', 'house' ) ); ?>
		</div><!-- .description -->

	<?php }

	// blog posts page - index.php
	elseif ( is_home() ) { ?>
		<h1 class="archive-title mb"><?php echo get_the_title( $globalSite['blog'] ); ?></h1>
	<?php }

	// category archive - category.php
	elseif ( is_category() ) { ?>
		<h1 class="archive-title mb"><?php single_cat_title(); ?></h1>

		<?php if ( category_description() && ! is_paged() ) : // Show an optional category description ?>
			<div class="description"><?php echo category_description(); ?></div>
		<?php endif; ?>
	<?php }

	// tag archive - tag.php
	elseif ( is_tag() ) { ?>
		<h1 class="archive-title"><?php single_tag_title(); ?></h1>

		<?php if ( tag_description() && ! is_paged() ) : // Show an optional tag description ?>
			<div class="description"><?php echo tag_description(); ?></div>
		<?php endif; ?>
	<?php }

	// taxonomy archive - taxonomy.php
	elseif ( is_tax() ) { ?>
		<h1 class="archive-title"><?php single_term_title(); ?></h1>

		<?php // get taxonomy description
			$description = term_description( '', get_query_var( 'taxonomy' ) );
		 	if ( $description && ! is_paged() ) : // Show an optional tag description ?>
				<div class="description"><?php echo $description; ?></div>
		<?php endif; ?>
	<?php }

	// author archive - author.php (fallback - archive.php)
	elseif ( is_author() ) { ?>

		<h1 class="archive-title fn n"><?php the_author_meta( 'display_name', get_query_var( 'author' ) ); ?></h1>

	<?php }

	// search results - search.php
	elseif ( is_search() ) { ?>

		<h1 class="page-title"><?php echo sprintf( __( 'Search results for "%s"', 'house' ), esc_attr( get_search_query() ) ); ?></h1>

		<div class="description">
			<?php echo wpautop( sprintf( __( 'You are browsing the search results for %s.', 'house' ), '<span class="search-query">' . esc_attr( get_search_query() ) . '</span>' ) ); ?>
		</div><!-- .description -->

	<?php }

	// custom post type archive - archive-{post-type}.php (fallback - archive.php)
	elseif ( is_post_type_archive() ) { ?>

		<?php $post_type = get_post_type_object( get_query_var( 'post_type' ) ); ?>

		<h1 class="archive-title"><?php post_type_archive_title(); ?></h1>

		<div class="description">
			<?php if ( ! empty( $post_type->description ) ) echo wpautop( $post_type->description ); ?>
		</div><!-- .description -->

	<?php }

	// date archives - date.php (fallback - archive.php)
	elseif ( is_date() ) {
			$day = get_the_time( 'd' );
			$month_num = get_the_time( 'm' );
			$month = $wp_locale->month[$month_num] . ' ';
			$year = get_the_time( 'Y' );

			if ( is_day() ) {
				$date = $month . $day . ', ' . $year;
			}
			elseif ( is_month() ) {
				$date = $month . $year;
			}
			elseif ( is_year() ) {
				$date = $year;
			}
		?>

		<h1 class="archive-title"><?php echo $date; ?></h1>

		<div class="description">
			<?php echo wpautop( sprintf( __( 'You are browsing the site archives for %s.', 'house' ), '<span class="date">' . $date . '</span>' ) ); ?>
		</div><!-- .description -->

	<?php }

	// archives fallback - archive.php
	elseif ( is_archive() ) { ?>

		<h1 class="archive-title"><?php _e( 'Archives', 'house' ); ?></h1>

		<div class="description">
			<?php echo wpautop( __( 'You are browsing the site archives.', 'house' ) ); ?>
		</div><!-- .description -->

	<?php }

	?>
</div>