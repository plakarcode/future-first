<?php
/**
 * Subscribe
 *
 * Template part for rendering subscribe form and social links
 *
 * @package WordPress
 */
?>
<section id="subscribe" class="subscribe">

	<div class="container">
		<div class="subscribe__content">
			<?php
				/**
				 * Get section title
				 */
				if ( get_field( 'subscribe_section_title', 'option' ) ) : ?>
					<h2><?php the_field( 'subscribe_section_title', 'option' ); ?></h2>
				<?php endif; // get_field( 'subscribe_section_title', 'option' )

				/**
				 * Get mailchimp form
				 */
				get_template_part( 'partials/content/mailchimp' );
			?>
		</div>
	</div>
</section><!-- subscribe -->
