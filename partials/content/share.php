<?php
/**
 * Share
 *
 * Template part for rendering sharng links.
 *
 * @package WordPress
 */
?>
<ul class="share">
	<li class="js-tooltip-wrap tooltip-wrap">
		<a href="javascript:;">
			<span>SHARE</span>
			<?php echo house_svg_icon( 'share' ); ?>
		</a>
		<ul class="tooltip">
			<?php
				/**
				 * If is tweet, share it's original url on twitter.com
				 */
				if ( get_post_format() === 'status' ) : ?>
				<li><?php share_link( 'facebook', custom_meta( 'tweet_permalink', false ) ); ?></li>
				<li><?php share_link( 'twitter', custom_meta( 'tweet_permalink', false ) ); ?></li>
			<?php else : ?>
				<li><?php share_link( 'facebook', get_permalink() ); ?></li>
				<li><?php share_link( 'twitter', get_permalink() ); ?></li>
			<?php endif; // get_post_format() === 'status' ?>
		</ul><!-- tooltip -->
	</li><!-- js-tooltip-wrap tooltip-wrap -->
</ul><!-- share -->