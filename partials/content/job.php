<?php
/**
 * Job
 *
 * Template part for rendering single job content on Jobs page.
 *
 * @package WordPress
 */
?>
<div class="jobs-boxes">
	<div class="jobs-boxes__item">

		<?php
			if ( get_field( 'jobs_download_button_label' ) ) {
				$label = get_field( 'jobs_download_button_label' );
			} else {
				$label = 'Download Application Pack';
			}
			/**
			 * Get file for download
			 */
			if ( get_field( 'job_application_pack' ) ) : ?>
				<a class="btn btn--primary" href="<?php the_field( 'job_application_pack' ); ?>"><?php echo $label; ?></a>
			<?php endif; // get_field( 'job_application_pack' ) ?>

		<div class="media">

			<div class="media__img">
				<time><?php echo get_the_date( 'd F Y' ); ?></time>
				<h2><?php the_title(); ?></h2>
			</div><!-- end of .media__img -->

			<div class="media__body">
				<?php
					/**
					 * Get visible content
					 */
					if ( get_field( 'job_content_visible' ) ) :
						the_field( 'job_content_visible' );
					endif; // get_field( 'job_content_visible' )

					/**
					 * Get invisible content
					 */
					if ( get_field( 'job_content_hidden' ) ) : ?>
						<div class="hidden-content">
							<?php the_field( 'job_content_hidden' ); ?>
						</div><!-- end of .hidden-content -->

						<a class="show-more" href="javascript:;">
							<?php echo house_svg_icon( 'plus' ); ?> <span>show MORE</span>
						</a>
				<?php endif; // get_field( 'job_content_hidden' ) ?>
			</div><!-- end of .media__body -->

		</div><!-- end of .media -->
	</div><!-- end of .jobs-boxes__item -->
</div><!-- end of .jobs-boxes -->