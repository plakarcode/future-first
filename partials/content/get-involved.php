<?php
/**
 * Get involved
 *
 * Template part for rendering get involved section.
 *
 * @package WordPress
 */
?>
<section id="get-involved" class="get-involved">
	<div class="container">
		<?php if ( get_field( 'involved_section_title' ) ) : ?>
			<header class="text-center">
				<h2><?php the_field( 'involved_section_title' ); ?></h2>
			</header>
		<?php endif; // get_field( 'involved_section_title' )?>

		<?php if ( have_rows( 'involved_section_repeater' ) ) : ?>

			<div class="layout">

				<?php while ( have_rows( 'involved_section_repeater' ) ) : the_row();
						/**
						 * Set the href
						 */
						if ( get_sub_field( 'link' ) ) {
							$href = get_sub_field( 'link' );
						} else {
							$href = 'javascript:;';
						} ?>

					<div class="layout__item large-and-up-1/3">
						<a href="<?php echo $href; ?>" class="get-involved__item" target="_blank">
							<?php
								/**
								 * Get the icon
								 */
								if ( get_sub_field( 'involved_select_icon' ) ) :
									echo house_svg_icon( get_sub_field( 'involved_select_icon' ) );
								endif; // get_sub_field( 'involved_select_icon' )
								/**
								 * Get the title
								 */
								if ( get_sub_field( 'title' ) ) : ?>
									<h3><?php the_sub_field( 'title' ); ?></h3>
								<?php endif; // get_sub_field( 'involved_select_icon' )

								/**
								 * Get description
								 */
								if ( get_sub_field( 'description' ) ) : ?>
									<p><?php the_sub_field( 'description' ); ?></p>
								<?php endif; // get_sub_field( 'description' )
							?>
						</a>
					</div><!-- end of .layout__item -->

				<?php endwhile; // have_rows( 'involved_section_repeater' ) ?>

			</div><!-- end of .layout -->

		<?php endif; // have_rows( 'involved_section_repeater' ) ?>

	</div><!-- end of .container -->
</section><!-- end of .get-involved -->
