<?php
/**
 * Related News
 *
 * Template part for rendering related news on single news post.
 *
 * @package WordPress
 */
if ( get_field( 'related_articles' ) ) :
	$related = get_field( 'related_articles' ); ?>

<section class="related-insights">
	<div class="container">
		<?php if ( get_field( 'related_articles_section_title' ) ) : ?>
			<h1 class="mb+"><?php the_field( 'related_articles_section_title' ); ?></h1>
		<?php endif; // get_field( 'related_articles_section_title' ) ?>

		<div class="layout">
			<?php foreach ( $related as $r ) :

				$id = $r->ID;
				// set the image
				if ( has_post_thumbnail( $id ) ) {
					$src = get_the_post_thumbnail_url( $id, 'large' );
				} else {
					$src = 'http://placehold.it/370x220';
				} ?>

				<div class="layout__item large-and-up-1/3">

					<?php if ( get_post_type() === 'post' ) : ?>

					<article class="related-insights__item">
						<a href="<?php echo get_permalink( $id ); ?>" class="img-wrap">
							<img src="<?php echo $src; ?>" alt="">
						</a>

						<a href="<?php echo get_permalink( $id ); ?>" class="related-insights__content">
							<h2><?php echo $r->post_title; ?></h2>
							<?php
								/**
								 * Prepare trimming the content
								 * @var string
								 */
								$content = wp_trim_words( $r->post_content, 30, '...' ); ?>

								<p><?php echo $content; ?></p>
						</a>

						<a href="<?php echo get_permalink( $id ); ?>" class="read-more">READ MORE</a>
					</article><!-- related-insights__item -->

					<?php elseif ( get_post_type() === 'press' ) : ?>

						<div class="press-grid__item">
							<div class="press-grid__logo-box">
								<img src="<?php echo $src; ?>">
							</div>
							<div class="text-wrap text-center">
								<time><?php echo get_the_date( 'j F Y' ); ?></time>
								<h4><?php echo $r->post_title; ?></h4>
								<hr>
								<a href="<?php echo get_permalink( $id ); ?>">Read MORE</a>
							</div>
						</div><!-- end of .press-grid__item -->

					<?php endif; // get_post_type() === 'post' ?>

				</div><!-- layout__item large-and-up-1/3 -->

			<?php endforeach; // ( $related as $r ) ?>
		</div><!-- layout-- >

	</div><!-- container -->
</section><!-- related-insights -->

<?php endif; // get_field( 'related_articles' )
