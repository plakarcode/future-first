<?php
/**
 * Stories
 *
 * Template part for rendering stories grid.
 *
 * @package WordPress
 */
$terms = get_terms( array(
    'taxonomy'   => 'type',
    'hide_empty' => false,
    'orderby'    => 'name',
    'order'      => 'DESC'
) );

if ( $terms ) : ?>

<div class="tabs tabs--magic">

	<ul class="tabs__nav js-line-following2">
		<?php foreach ( $terms as $term ) : ?>
		<li><a href="#tab-<?php echo $term->slug; ?>"><?php echo $term->name; ?></a></li>
		<?php endforeach; // $terms as $term ?>
	</ul>


	<?php foreach ( $terms as $term ) : ?>
		<div id="tab-<?php echo $term->slug; ?>" class="tabs__item">
			<div class="employer-stories">
				<h1><?php echo $term->name; ?></h1>

			<?php
				/**
				 * Get stories
				 * @var obj|Error
				 */
				$stories = get_stories( $term->slug );
				
				if ( $stories->have_posts() ) : ?>

					<div class="layout">

						
							<?php while ( $stories->have_posts() ) : $stories->the_post();
								if ( has_post_thumbnail() ) {
									$style = 'style="background-image: url( ' . get_the_post_thumbnail_url( get_the_ID(), 'large' ) . ');"';
								} else {
									$style = 'style="background-image: url(http://placehold.it/370x220);"';
								} ?>

									<div class="layout__item large-and-up-1/3 medium-and-up-1/2">
										<div class="employer-stories__wrap">
											<div class="img-wrap" <?php echo $style; ?>></div>
											<div class="text-wrap text-center">
												<h2><?php the_title(); ?></h2>
												<time><?php echo get_the_date( 'd F Y' ); ?></time>
												<p><?php
													// translators: number of words, append to content
													echo content_trim_words( 16, '...' ); ?></p>
												<a href="#our-stories-<?php the_ID(); ?>" class="openpopup read-more employer-stories__more">Read MORE</a>
											</div><!-- text-wrap text-center -->
										</div><!-- /.employer-stories__wrap -->
									</div><!-- end of .employer-stories__item -->

									<div id="our-stories-<?php the_ID(); ?>" class="our-stories-lightbox magnific-popup mfp-hide magnific-animate">
										<div class="our-stories-lightbox__img" <?php echo $style; ?>></div>

										<div class="our-stories-lightbox__content">
											<div class="our-stories-lightbox__content-title">
												<p><?php echo get_the_date( 'd F Y' ); ?></p>
												<h1><?php the_title(); ?></h1>
											</div><!-- our-stories-lightbox__content-title -->

											<div class="our-stories-lightbox__content-main">
												<?php
													/**
													 * Get flexible content fields if any,
													 * otherwise get regular content
													 */
													if ( get_field( 'story_popup_content' ) ) :

														get_template_part( 'partials/content/story-sections' );

													else :

														the_content();

													endif; // get_field( 'content_fields' )
												?>
											</div><!-- our-stories-lightbox__content-main -->
										</div><!-- our-stories-lightbox__content -->

										<div class="mfp-close">
											<span>close</span>
											<?php echo house_svg_icon( 'close' ); ?>
										</div><!-- mfp-close -->
									</div><!-- our-stories-lightbox magnific-popup mfp-hide magnific-animate -->

							<?php endwhile; // $stories->have_posts() ?>
						

					</div><!-- layout -->

				<?php endif; // $stories->have_posts() ?>

			</div><!-- employer-stories -->
		</div><!-- #tab-<?php echo $term->slug; ?> tabs__item -->
	<?php endforeach; // $terms as $term ?>

</div><!-- tabs tabs--magic -->

<?php endif; //  $terms