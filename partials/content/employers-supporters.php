<?php
/**
 * Employers and Supporters
 *
 * Template part for rendering supporters section.
 *
 * @package WordPress
 */
?>
<div class="employers__grid tabs tabs--magic mv+">
	<ul class="tabs__nav js-line-following2">
		<li class='is-active'><a href="#tab-employers">Employers & Supporters</a></li>
	</ul><!-- tabs__nav js-line-following2 -->

	<div class="tabs__item" id="tab-employers">
		<?php get_template_part( 'partials/content/employers' ); ?>
	</div><!-- tabs__item -->
</div><!-- employers__grid tabs tabs--magic mt+ -->