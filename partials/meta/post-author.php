<?php
/**
 * Author
 *
 * Template part for rendering author's info on posts
 * if a user has filled out their description and
 * this is a multi-author blog.
 *
 * @package WordPress
 */

if ( is_singular() ) : ?>

<div class="layout mt+ author-box" id="author-box">
	<div class="layout__item medium-and-up-1/4 author-box-image">
		<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'house_author_bio_avatar_size', 210 ) ); ?>
	</div><!-- layout__item medium-and-up-1/4 author-box-image -->

	<div class="layout__item medium-and-up-3/4">
		<h2 class="author-box-name"><?php the_author_meta( 'display_name' ); ?></h2>
		<p>AUTHOR</p>

		<?php if ( get_the_author_meta( 'description' ) ) : ?>
			<p><?php the_author_meta( 'description' ); ?></p>
		<?php endif; // get_the_author_meta( 'description' ) ?>

		<ul class="list-inline">
			<li><p>City: <span><?php the_author_meta( 'city' ); ?></span></p></li>
			<li><p>Email: <span><?php the_author_meta( 'user_email' ); ?></span></p></li>
			<li><p>Phone: <span><?php the_author_meta( 'phone' ); ?></span></p></li>
		</ul><!-- list-inline -->
	</div><!-- layout__item medium-and-up-3/4 -->
</div><!-- layout mt+ author-box -->

<?php endif; //  is_singular()