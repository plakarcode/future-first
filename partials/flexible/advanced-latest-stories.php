<?php
/**
 * Latest Stories
 *
 * Template part for rendering ACF advanced flexible sections - latest stories
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
$stories = get_stories( get_sub_field( 'type' ), 3 );
?>

<div class="bgr-light-gray what-we-do__stories pv+ desktop-and-up-pv++">
	<div class="container">

		<?php if ( get_sub_field( 'title' ) || get_sub_field( 'content' ) ) : ?>
		<div class="container container--small pb text-center">

				<?php if ( get_sub_field( 'title' ) ) : ?>
					<h2><?php the_sub_field( 'title' ); ?></h2>
				<?php endif; // get_sub_field( 'title' )

				if ( get_sub_field( 'content' ) ) : ?>
					<p><?php the_sub_field( 'content' ); ?></p>
				<?php endif; // get_sub_field( 'content' )

				if ( get_sub_field( 'content_link_url' ) ) : ?>
					<a href="<?php the_sub_field( 'content_link_url' ); ?>" class="btn btn--primary btn--border btn--gray"><?php the_sub_field( 'content_link_title' ); ?></a>
				<?php endif; // get_sub_field( 'content_link_url' ) ?>
		</div><!-- /.container container--small -->
		<?php endif; //  get_sub_field( 'title' ) || get_sub_field( 'content' )

		if ( $stories->have_posts() ) : ?>
			<div class="layout">

				<?php while ( $stories->have_posts() ) : $stories->the_post();
						if ( has_post_thumbnail() ) {
							$style = 'style="background-image: url( ' . get_the_post_thumbnail_url( get_the_ID(), 'large' ) . ');"';
						} else {
							$style = 'style="background-image: url(http://placehold.it/370x220);"';
						} ?>
						<div class="employer-stories__item">
							<div class="employer-stories__wrap">
								<div class="img-wrap" <?php echo $style; ?>></div>
								<div class="text-wrap text-center">
									<h2><?php the_title(); ?></h2>
									<time><?php echo get_the_date( 'j F Y' ); ?></time>
									<p><?php
										// translators: number of words, append to content
										echo content_trim_words( 16, '...' ); ?></p>
									<a href="#our-stories-<?php the_ID(); ?>" class="openpopup read-more employer-stories__more">Read MORE</a>
								</div>
							</div><!-- end of .employer-stories__wrap -->
						</div><!-- end of .employer-stories__item -->

					<div id="our-stories-<?php the_ID(); ?>" class="our-stories-lightbox magnific-popup mfp-hide magnific-animate">
						<div class="our-stories-lightbox__img" <?php echo $style; ?>></div>

						<div class="our-stories-lightbox__content">
							<div class="our-stories-lightbox__content-title">
								<p><?php echo get_the_date( 'd F Y' ); ?></p>
								<h1><?php the_title(); ?></h1>
							</div><!-- our-stories-lightbox__content-title -->

							<div class="our-stories-lightbox__content-main">
								<?php
									/**
									 * Get flexible content fields if any,
									 * otherwise get regular content
									 */
									if ( get_field( 'story_popup_content' ) ) :

										get_template_part( 'partials/content/story-sections' );

									else :

										the_content();

									endif; // get_field( 'content_fields' )
								?>
							</div><!-- our-stories-lightbox__content-main -->
						</div><!-- our-stories-lightbox__content -->

						<div class="mfp-close">
							<span>close</span>
							<?php echo house_svg_icon( 'close' ); ?>
						</div><!-- mfp-close -->
					</div><!-- our-stories-lightbox magnific-popup mfp-hide magnific-animate -->
					
				<?php endwhile; ?>
			</div><!-- layout -->
			<?php endif; ?>
			<?php wp_reset_query(); ?>

		<?php if ( get_sub_field( 'stories_page_link' ) ) : ?>
		<div class="mt+ text-center">
			<a href="<?php the_sub_field( 'stories_page_link' ); ?>" class="btn btn--primary">View More Stories</a>
		</div>
		<?php endif; // get_sub_field( 'stories_page_link' ) ?>
	</div>
</div>
