<?php
/**
 * Gallery
 *
 * Template part for rendering ACF flexible sections - gallery
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
if ( ! get_sub_field( 'gallery' ) ) {
	return;
}

	$gallery = get_sub_field( 'gallery' );

	foreach ( $gallery as $image ) :

		/**
		 * Prepare sizes
		 * @var string
		 */
		$thumbnail = $image['sizes']['thumbnail'];
		$medium    = $image['sizes']['medium'];
		$large     = $image['sizes']['large'];
		$full      = $image['url'];	?>

		<img src="<?php echo $thumbnail; ?>" >

	<?php endforeach; // $gallery as $image
