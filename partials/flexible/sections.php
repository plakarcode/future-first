<?php
/**
 * Flexible sections
 *
 * Template part for rendering ACF flexible sections
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
while ( the_flexible_field( 'content_fields' ) ) :

	// START TEXT INTRO SECTION
	if ( get_row_layout() == 'text_intro' ) :
		get_template_part( 'partials/flexible/section', 'text-intro' );
	// END TEXT INTRO SECTION

	// START IMAGE INTRO SECTION
	elseif ( get_row_layout() == 'image_intro' ) :
		get_template_part( 'partials/flexible/section', 'image-intro' );
	// END IMAGE INTRO SECTION

	// START TYPOGRAPHY SECTION
	elseif ( get_row_layout() == 'typography' ) :
		get_template_part( 'partials/flexible/section', 'typography' );
	// END TYPOGRAPHY SECTION

	// START STORY TEXT SECTION
	elseif ( get_row_layout() == 'story_text' ) :
		get_template_part( 'partials/flexible/section', 'story-text' );
	// END STORY TEXT SECTION

	// START GALLERY SECTION
	elseif ( get_row_layout() == 'gallery_layout' ) :
		get_template_part( 'partials/flexible/section', 'gallery' );
	// END GALLERY SECTION

	// START IMAGE SLIDER SECTION
	elseif ( get_row_layout() == 'image_slider_section' ) :
		get_template_part( 'partials/flexible/section', 'image-slider' );
	// END IMAGE SLIDER SECTION

	// START NUMBERED BLOCKS SECTION
	elseif ( get_row_layout() == 'advanced_sections_layout' ) :
		get_template_part( 'partials/flexible/sections', 'advanced' );
	// END NUMBERED BLOCKS SECTION

	endif; // get_row_layout()

endwhile; // the_flexible_field( 'content_fields' )