<?php
/**
 * Map Tabs
 *
 * Template part for rendering ACF flexible sections - map tabs
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */

$tabs = get_sub_field( 'tabs' );
?>
<div class="container">
<div class="contact__tabs mb+ desktop-and-up-mb++">

	<div class="tabs tabs--magic">
		<ul class="tabs__nav js-line-following2">
			<?php $first_class = "class='is-active'"; foreach ( $tabs as $tab ) : ?>
				<li <?php echo $first_class; ?>><a href="#tab-<?php echo sanitize_title( $tab['title'] ); ?>"><?php echo $tab['title']; ?></a></li>
			<?php $first_class = ''; endforeach; ?>
		</ul>

		<?php foreach ( $tabs as $tab ) : ?>

			<?php global $globalSite; $current_map = ++$globalSite['current_map']; ?>

			<div id="tab-<?php echo sanitize_title( $tab['title'] ); ?>" class="tabs__item">
				<div class="container">
					<div id="map_canvas_<?php echo $current_map; ?>" class="mb desktop-and-up-mb+"></div>
				</div>

				<div class="layout mt+">
					<div class="layout__item large-and-up-1/3 text-center">
						<?php if ( $tab['address'] ) : ?>
							<div class="contact-icon text-center">
								<p><?php echo house_svg_icon( 'address' ); ?></p>
							</div>
							<p><?php echo $tab['address']; ?></p>
						<?php endif; // isset( $tab['address'] ) ?>
					</div>
					<div class="layout__item large-and-up-1/3 text-center">
						<?php if ( $tab['phone_number'] ) : ?>
							<div class="contact-icon text-center">
								<p><?php echo house_svg_icon( 'phone' ); ?></p>
							</div>
							<p><?php echo $tab['phone_number']; ?></p>
						<?php endif; // isset( $tab['phone_number'] ) ?>
					</div>
					<div class="layout__item large-and-up-1/3 text-center">
						<?php if ( $tab['email_address'] ) : ?>
							<div class="contact-icon text-center">
								<p><?php echo house_svg_icon( 'email' ); ?></p>
							</div>
							<p><?php echo $tab['email_address']; ?></p>
						<?php endif; // isset( $tab['email_address'] ) ?>
					</div>
				</div>
			</div>

		<?php endforeach; ?>

	</div>

</div>
</div>