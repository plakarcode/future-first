<?php
/**
 * Template for press grid section
 */
$items = get_sub_field( 'items' );
?>

<div class="icon-slider pb+">
	<div class="container">
		<h2><?php the_sub_field( 'title' ); ?></h2>
		<div class="alumni-slider owl-carousel">
			<?php $icon_count = 1; foreach ( $items as $item ) : ?>
				<div class="alumni-slider__item">
					<div class="alumni__img">
						<?php echo house_svg_icon( $item['icon'], 'icon-students' ); ?>
					</div><!-- /.alumni__img -->
					<div class="alumni__number"><span><?php echo str_pad( $icon_count, 2, '0', STR_PAD_LEFT ); ?></span></div><!-- /.alumni__number -->
					<div class="alumni__text">
						<h4><?php echo $item['title']; ?></h4>
						<p><?php echo $item['intro_text']; ?></p>
						<div class="hidden-content">
							<p><?php echo $item['extra_text']; ?></p>
						</div><!-- end of .hidden-content -->
						<a href="" class="show-more more-info"><span>Show more</span></a>
					</div><!-- /.alumni__text -->
				</div><!-- /.alumni-slider__item -->
			<?php $icon_count++; endforeach; ?>
		</div><!-- /.alumni-slider -->
	</div><!-- /.container -->
</div><!-- /.icon-slider -->