<?php
/**
 * Template for our people flexible section
 */
$people = get_people();

if ( $people ) : ?>

<div class="people__team-section large-and-up-p++ medium-and-up-p p-">
	<div class="tabs tabs--magic">
		<?php if ( count( $people ) > 1 ) : ?>
			<ul class="tabs__nav js-line-following2">
				<?php $first_class = 'is-active'; foreach ( $people as $type => $group ) : ?>
					<li class="<?php echo $first_class; ?>"><a href="#tab-<?php echo sanitize_title( $type ); ?>"><?php echo ucwords($type); ?></a></li>
				<?php $first_class = ''; endforeach; ?>
				<li class="magic-line2"></li>
			</ul>
		<?php endif; ?>
		<?php $first_class = 'active-tab'; $first_style = 'display: block;'; foreach ( $people as $type => $group ) : ?>
			<div id="tab-<?php echo sanitize_title( $type ); ?>" class="tabs__item <?php echo $first_class; ?>" style="<?php echo $first_style; ?>">
				<?php if ( count( $people ) > 1 ) : ?>
					<div class="justifize mb">
						<div class="justifize__box small-1/1">
							<h2 class="#"><?php echo ucwords( $type ); ?></h2>
						</div>
						<div class="justifize__box small-1/1">
						</div>
					</div>
				<?php endif; ?>
				<div class="layout">

					<?php foreach ( $group as $member ) : $image = $member['image']; $lightbox_id = "people-lightbox-" . sanitize_title($member['name']); ?>

						<div class="layout__item 1/1 medium-and-up-1/2 extralarge-and-up-1/3 medium-and-up-mb mb--">
							<a href="#<?php echo $lightbox_id; ?>" class="pink-box pink-box--people openpopup" style="background-image: url('<?php echo $image['sizes']['large']; ?>');"></a>
							<div class="thumbnail thumbnail--people">
								<a href="#<?php echo $lightbox_id; ?>" class="openpopup"><h3><?php echo $member['name']; ?></h3></a>
								<p class="subtitle"><?php echo $member['role']; ?></p>
								<hr class="break-small">
								<p><?php echo strtolower( $member['email'] ); ?></p>
							</div>
						</div>

					<?php endforeach; ?>

				</div>
			</div>
		<?php $first_class = $first_style = ''; endforeach; ?>
	</div>
</div>

<?php foreach ( $people as $type => $group ) : ?>
	<?php foreach ( $group as $member ) : $image = $member['image']; $lightbox_id = "people-lightbox-" . sanitize_title( $member['name'] ); ?>
		<div id='<?php echo $lightbox_id; ?>' class="people-lightbox magnific-popup mfp-hide magnific-animate">
			<div class="people-lightbox__content pv large-and-up-pv+ desktop-and-up-pv++ ph desktop-and-up-ph++">
				<div class="layout">
					<div class="layout__item large-and-up-5/12 people-lightbox__content-title">
						<div class="avatar medium-and-up-mb mb0" style="background-image: url('<?php echo $image['sizes']['medium']; ?>')">
						</div>
					</div>
					<div class="layout__item large-and-up-7/12 people-lightbox__content-main">
						<?php if ( is_array( $member['links'] ) ) : ?>
							<ul class="people-lightbox__social large-and-up-pl-">
								<?php foreach( $member['links'] as $link ) : ?>
									<li><a href="<?php echo $link['url']; ?>"><?php echo house_svg_icon( strtolower( $link['type'] ) ); ?></a></li>
								<?php endforeach; ?>
							</ul>
						<?php endif; ?>
						<h1 class="mb-- text-center"><?php echo $member['name']; ?></h1>
						<p class="subtitle text-center"><?php echo $member['role']; ?></p>
						<hr class="break-small">
						<p class="mb-- text-center"><?php echo strtolower( $member['email'] ); ?></p>
						<p class="large-and-up-pl-"><?php echo $member['description']; ?></p>
					</div>
				</div>
			</div>
			<div class="mfp-close"><span>close</span> <?php echo house_svg_icon( 'close') ; ?></div>
		</div>
	<?php endforeach; ?>
<?php endforeach; ?>

<?php endif; // $people ?>