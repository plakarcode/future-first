<?php
/**
 * Easy map
 *
 * Template part for rendering ACF flexible sections - easy map
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
if ( get_sub_field( 'map_url' ) ) : ?>
<div class="container">
	<iframe class="easy-map" src="<?php the_sub_field( 'map_url' ); ?>"></iframe>
</div>
<?php endif; // get_sub_field( 'map_url' )