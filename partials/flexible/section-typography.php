<?php
/**
 * Template for typography flexible section
 */
?>

<section class="typography">
	<div class="alumni__why container container--small">
		<?php if ( get_sub_field( 'title' ) ) : ?>
		<h2><?php the_sub_field( 'title' ); ?></h2>
		<?php endif; //  get_sub_field( 'title' )

		if ( get_sub_field( 'content' ) ) :
			the_sub_field( 'content' );
		endif; // get_sub_field( 'content' ) ?>
	</div>
</section>
<!-- typography -->