<?php
/**
 * Template for what we do steps section
 */
?>

<div class="what-we-do__steps">

	<div class="container hide-md">
	  	<div class="steps-grid pv+ desktop-and-up-pv++">
	    	<h2><?php the_sub_field( 'title' ); ?></h2>
	    		<div class="layout">

					<?php 
						if ( have_rows( 'steps' ) ) : while( have_rows( 'steps' ) ) : the_row(); 
						$position_icon = get_sub_field( 'position_icon' ); 
					?>

						<?php if ( $position_icon == 'left' ) : ?>
			      			<div class="layout__item large-and-up-1/4">
			        			<img src="<?php the_sub_field( 'icon' ); ?>" class="">
			      			</div>
			      			<div class="layout__item large-and-up-3/4">
			        			<h3><?php the_sub_field( 'title' ); ?></h3>
			        			<p><?php the_sub_field( 'content' ); ?></p>
			      			</div>
						<?php else: ?>
			      			<div class="layout__item large-and-up-3/4">
			        			<h3><?php the_sub_field( 'title' ); ?></h3>
			        			<p><?php the_sub_field( 'content' ); ?></p>
			      			</div>
							<div class="layout__item large-and-up-1/4">
			        			<img src="<?php the_sub_field( 'icon' ); ?>" class="">
			      			</div>
			      		<?php endif; ?>

					<?php endwhile; endif; ?>

	    		</div>
	  	</div>
	</div>

	<div class="container hide-lg">
	  	<div class="steps-grid pv+">
	    	<h2><?php the_sub_field( 'title' ); ?></h2>
	    		<div class="layout text-center">

					<?php if ( have_rows( 'steps' ) ) : while( have_rows( 'steps' ) ) : the_row(); ?>
	      			<div class="layout__item">
	        			<img src="<?php the_sub_field( 'icon' ); ?>" class="">
	      			</div>
	      			<div class="layout__item">
	        			<h3><?php the_sub_field( 'title' ); ?></h3>
	        			<p><?php the_sub_field( 'content' ); ?></p>
	      			</div>
					<?php endwhile; endif; ?>

	    		</div>
	  	</div>
	</div>

</div>