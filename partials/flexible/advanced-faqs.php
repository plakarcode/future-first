<?php
/**
 * Tabs FAQs
 *
 * Template part for rendering ACF flexible sections - faqs in tabs
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
$tabs = get_sub_field( 'faq_wrap' );
if ( $tabs ) : ?>
<section class="faq__questions">
	<div class="container">
		<div class="tabs tabs--magic">

			<ul class="tabs__nav js-line-following2">
				<?php foreach ( $tabs as $tab ) : ?>
				<li><a href="#tab-<?php echo sanitize_title_with_dashes( $tab['title'] ); ?>"><?php echo $tab['title']; ?></a></li>
				<?php endforeach; // $tabs as $tab ?>
			</ul>

			<?php foreach ( $tabs as $tab ) : ?>

				<div id="tab-<?php echo sanitize_title_with_dashes( $tab['title'] ); ?>" class="tabs__item">
					<div class="accordion">

					<?php
						/**
						 * Get FAQs
						 * @var array
						 */
						$faqs = $tab['house_faq_items'];

						if ( $faqs ) :
							foreach ( $faqs as $faq ) : ?>

								<div class="accordion__item">
									<h2 class="accordion__item-title">
										<a href=""><?php echo $faq['house_faq_item_question']; ?></a>
										<span class="icon-arrow">
											<?php echo house_svg_icon( 'arrow-right-2', 'icon-arrow-down' ); ?>
										</span>
									</h2>
									<div class="accordion__item-content">
										<?php echo $faq['house_faq_item_answer']; ?>
									</div>
								</div>

							<?php endforeach; // $faqs as $faq
						endif; // $faqs	?>

					</div><!-- accordion -->
				</div><!-- tabs__item -->

			<?php endforeach; // $tabs as $tab ?>

		</div><!-- tabs tabs--magic -->

		<?php
			/**
			 * Get additional contact info
			 */
			if ( get_sub_field( 'additional_contact_info_email' ) ) : ?>
			<p class="desktop-and-up-mt+ mt faq__questions--contact">
				We’re happy to answer any additional questions you have. You can email <a href="mailto:<?php the_sub_field( 'additional_contact_info_email' ); ?>"><?php the_sub_field( 'additional_contact_info_email' ); ?></a>
				<?php if ( get_sub_field( 'additional_contact_info_phone' ) ) : ?>
					or call <span><?php the_sub_field( 'additional_contact_info_phone' ); ?></span> and speak to a member of our team.
				<?php endif; // get_sub_field( 'additional_contact_info_phone' ) ?>
			</p>
		<?php endif; // get_sub_field( 'additional_contact_info_email' ) ?>

	</div><!-- container -->
</section><!-- faq__questions -->

<?php endif; //$tabs