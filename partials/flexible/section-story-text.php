<?php
/**
 * Template for story text flexible section
 */
?>
<div class="container">
	<div class="story-text">
		<div class="school-registration__text-box text-box--center">
			<?php if ( get_sub_field( 'title' ) ) : ?>
			<h1 class="mb+"><?php the_sub_field( 'title' ); ?></h1>
			<?php endif; // get_sub_field( 'title' )

			if ( get_sub_field( 'upper_text' ) ) :
				the_sub_field( 'upper_text' );
			endif; // get_sub_field( 'upper_text' );  ?>
		</div>

		<?php if ( get_sub_field( 'pull_quote' ) ) : ?>
		<div class="container">
			<div class="highlight"><?php the_sub_field( 'pull_quote' ); ?></div>
		</div>
		<?php endif; // get_sub_field( 'pull_quote' )

		if ( get_sub_field( 'lower_text' ) ) : ?>
		<div class="school-registration__text-box text-box--center">
			<?php the_sub_field( 'lower_text' ); ?>
		</div>
		<?php endif; // get_sub_field( 'lower_text' ) ?>
	</div>
</div>