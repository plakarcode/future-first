<?php
/**
 * Template for text membership types section
 */
$types = get_sub_field( 'types' );
?>

<div class="bgr-light-gray pv+ desktop-and-up-pv++">
	<div class="container">
		<div class="join-us">
			
			<?php foreach ( $types as $type ) : ?>
			
				<div class="join-us__item">
					<div class="join-us__wrap">
						<div class="join-us__top">

							<?php echo house_svg_icon( '' . $type["icon"] . '' ); ?>

						</div><!-- /.join-us__icon -->
						<div class="join-us__info">
							<div class="join-us__package-name">
								<h2><?php echo $type['name']; ?></h2>
								<p><?php echo $type['description']; ?></p>
							</div><!-- /.join-us__package-name -->
							<?php foreach ( $type['details'] as $detail ) : ?>
							<div class="accordion__item">
								<h2 class="accordion__item-title">
									<a href=""><?php echo $detail['title']; ?></a>
									<span class="icon-arrow">
										<?php echo house_svg_icon( 'arrow-right-2', 'icon-arrow-down' ); ?>
									</span>
								</h2>
								<div class="accordion__item-content">
									<?php echo $detail['bullet_point']; ?>
								</div>
							</div>
							<?php endforeach; ?>
						</div><!-- /.join-us__info -->
						<a href="<?php echo $type['url']; ?>" class="join-us__more">Find out more</a>
					</div><!-- /.join-us__wrap -->
				</div><!-- /.join-us__item -->
				
			<?php endforeach; ?>
				
		</div><!-- /.join-us -->
	</div>
</div>
<!-- membership types -->