<?php
/**
 * Template for numbered blocks flexible section
 */
if ( ! get_sub_field( 'blocks' ) ) {
	return;
}

$blocks = get_sub_field( 'blocks' );
$count = 0;
?>

<div class="list-block mt+">

	<?php foreach ($blocks as $block): $count++; ?>

		<div class="list-block__element">
			<div class="container">
				<div class="list-block__count <?php if ( $count % 2 == 0 ) { echo 'list-block__count--right'; }?>">
					<?php echo str_pad( $count, 2, '0', STR_PAD_LEFT ); ?>
				</div>

				<?php if ( $block['image'] ) : ?>
				<img class="list-block__img <?php if ( $count % 2 == 0 ) { echo 'list-block__img--right'; }?>" src="<?php echo $block['image']['sizes']['large']; ?>" alt="">
				<?php endif; // $block['image'] ?>

				<div class="list-block__box <?php if ( $count % 2 != 0 ) { echo 'list-block__box--right'; }?>">
					<?php if ( $block['title'] ) : ?>
					<h2><?php echo $block['title']; ?></h2>
					<?php endif; // $block['title']

					if ( $block['content'] ) : ?>
					<p><?php echo $block['content']; ?></p>
					<?php endif; // content ?>
					<a href="">+ Show more</a>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>

	<?php endforeach; ?>

</div>
<!-- numbered-blocks -->