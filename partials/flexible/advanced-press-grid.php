<?php
/**
 * Template for press grid section
 *
 * @package WordPress
 */
$press = get_press();

if ( $press->have_posts() ) : ?>

<section class="press bgr-gray pv+ desktop-and-up-pv++">
	<div class="container">

        <div class="layout press-grid">
			<?php while ( $press->have_posts() ) : $press->the_post(); ?>

				<div class="layout__item large-and-up-1/3">
					<div class="press-grid__item">
						<div class="press-grid__logo-box" style="background-image: url(<?php the_post_thumbnail_url(); ?>);">
							<?php if ( get_field( 'sticky', get_the_ID() == '1' ) ) : ?>
								<div class="pinned-item">
									<?php echo house_svg_icon( 'pin' ); ?>
								</div><!-- pinned-item -->
							<?php endif; // get_field( 'sticky', get_the_ID() == '1' ) ?>
							<img src="<?php the_post_thumbnail_url(); ?>">
						</div><!-- press-grid__logo-box -->

						<div class="text-wrap text-center">
							<time><?php echo get_the_date( 'j F Y' ); ?></time>
							<h4><?php the_title(); ?></h4>
							<hr>
							<a href="<?php the_permalink(); ?>" class="read-more">Read more</a>
						</div><!-- text-wrap text-center -->
					</div><!-- end of .press-grid__item -->
				</div><!-- layout__item large-and-up-1/3 -->

			<?php endwhile; // $press->have_posts() ?>
		</div><!-- layout press-grid -->

		<div class="text-center">
			<?php house_simple_content_pagination( 'pagination', $press, 'pagination' ); ?>
		</div><!-- text-center -->

	</div><!-- container -->
</section><!-- press bgr-gray pv+ -->

<?php endif; // $press->have_posts()