<?php
/**
 * Template for image intro flexible section
 */
/**
 * Get the image
 * @var string
 */
$background = get_sub_field( 'background' );
/**
 * Get text box positioning
 * @var string
 */
$text_vertical = get_sub_field( 'text_box_vertical_position' );
$text_horizontal = get_sub_field( 'text_box_horizontal_position' );
$text_alignment = get_sub_field( 'text_box_text_alignment' );
/**
 * Complete box vertical positioning - left, right or center
 * Default is left
 */
if ( $text_vertical ) {
	$vertical = 'text-box--' . $text_vertical;
}
/**
 * Complete box horizontal positioning - top or bottom
 * Default is bottom
 */
if ( $text_horizontal === 'bottom' ) {
	$horizontal = ' text-box--small';
} else {
	$horizontal = '';
}
/**
 * Text alignment only - left, right or center
 * Default is left
 */
if ( $text_alignment === 'right' || $text_alignment === 'center' ) {
	$alignment = ' text-' . $text_alignment;
} else {
	$alignment = '';
}
/**
 * Build the class names, will be used for '.text-box' <div>
 * @var string
 */
$extra_class = $vertical . $horizontal . $alignment;


/**
 * Set '.intro' <div> class based on text box position
 */
if ( $text_vertical === 'centre' ) {
	$intro = $text_vertical;
} else {
	$intro = 'large';
} ?>

<div class="container">
	<div class="intro intro--<?php echo $intro; ?>">

		<?php if ( $background ) : ?>
			<img src="<?php echo $background['sizes']['large']; ?>" alt="">
		<?php endif; // $background ?>

		<div class="text-box <?php echo $extra_class; ?>">
			<?php if ( get_sub_field( 'title' ) ) : ?>
			<h1 class="h2"><?php the_sub_field( 'title' ); ?></h1>
			<?php endif; // get_sub_field( 'title' )

			if ( get_sub_field( 'lead' ) ) : ?>
			<div class="lead"><?php the_sub_field( 'lead' ); ?></div>
			<?php endif; // get_sub_field( 'lead' )

			if ( get_sub_field( 'description' ) ) : ?>
			<p><?php the_sub_field( 'description' ); ?></p>
			<?php endif; // get_sub_field( 'description' ) ?>
		</div><!-- text-box <?php echo $extra_class; ?> -->
	</div>
</div><!-- intro intro--<?php echo $intro; ?> -->