<?php
/**
 * Template for press grid section
 */
$items = get_sub_field('items');
?>

<div class="container container--small pv+ desktop-and-up-pb++">
	<div class="layout pt">

		<?php $id = 0; ?>
		<?php foreach ($items as $item) : ?>
		<?php $id++; ?>
	    <div class="layout__item medium-and-up-1/4">
	        <div class="alumni-item">
	            <a href="#<?php echo $id; ?>" class="openpopup"><span>
					<?php echo house_svg_icon($item['icon']); ?>
				</span></a>
	            <h3><?php echo $item['title']; ?></h3>
	        </div><!-- /.alumni-item -->
	    </div><!-- /.layout__item -->
		<?php endforeach; ?>

	</div>
</div><!-- /.container -->

<!-- LIGHTBOXES -->
<?php $id = 0; ?>
<?php foreach ($items as $item) : ?>
<?php $id++; ?>
<div id='<?php echo $id; ?>' class="alumni-lightbox magnific-popup mfp-hide magnific-animate">
  	<div class="alumni-lightbox__content">
    
  		<?php echo house_svg_icon($item['icon']); ?>

    	<h2><?php echo $item['title']; ?></h2>
    	<p><?php echo $item['content']; ?></p>
  	</div>
  	<div class="mfp-close"><span>close</span> 
  		<svg role="img" class="icon icon-close">
    		<use xlink:href="<?php echo get_template_directory_uri(); ?>/icons/icons.svg#icon-close"></use>
  		</svg>
	</div>
</div>
<?php endforeach; ?>