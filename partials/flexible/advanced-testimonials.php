<?php
/**
 * Testimonials
 *
 * Template part for rendering ACF advanced flexible sections - testimonials
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
$items = get_sub_field('items');
?>

<div class="pv+ desktop-and-up-pv++">
	<div class="join-us__grid">
		<div class="container">
			<h2 class="text-center mb+"><?php the_sub_field( 'title' ); ?></h2>
			<div class="layout">
				<?php foreach( $items as $item ) : ?>
					<div class="layout__item large-and-up-1/2">
						<div class="layout">
							<div class="layout__item large-and-up-1/6">
								<div class="join-us__grid--avatar" style="background-image:url('<?php echo $item['image']['sizes']['thumbnail']; ?>')"></div>
							</div>
							<div class="layout__item large-and-up-5/6">
								<div class="join-us__grid--text">
									<p><?php echo $item['quote']; ?></p>
									<p><?php echo $item['author']; ?></p>
									<p><?php echo $item['role']; ?></p>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>
