<?php
/**
 * Template for text intro flexible section
 *
 * @package WordPress
 */
?>

<section class="<?php the_sub_field( 'text_alignment' ); ?> mb+ desktop-and-up-mb++">
	<div class="container">

		<?php if ( get_sub_field( 'title' ) ) : ?>
			<h1 class="mb- desktop-and-up-mb"><?php the_sub_field('title'); ?></h1>
		<?php endif; // get_sub_field( 'title' )

		if ( get_sub_field( 'lead' ) ) : ?>
			<div class="lead mb"><?php the_sub_field('lead'); ?></div>
		<?php endif; // get_sub_field( 'lead' )

		if ( get_sub_field( 'content' ) ) : ?>
			<div class="simple-intro__text">
				<?php the_sub_field('content'); ?>
			</div><!-- simple-intro__text -->
		<?php endif; // get_sub_field( 'content' ) ?>

	</div><!-- container -->
</section><!-- <?php the_sub_field( 'text_alignment' ); ?> mb+ -->