<?php
/**
 * Horizontal rule
 *
 * Template part for rendering ACF flexible sections - hr
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
?>
<hr>