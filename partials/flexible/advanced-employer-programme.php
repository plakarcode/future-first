<?php
/**
 * Template for Employer programme section
 */
?>

<div class="employer-partners">
    <div class="container">
      	<div class="container container--small">
        	<div class="text-center">

        		<?php if ( get_sub_field( 'title' ) || get_sub_field( 'content' ) ) : ?>
          		<h2><?php the_sub_field( 'title' ); ?></h2>
          		<?php the_sub_field( 'content' ); ?>
	          	<?php endif; ?>

          		<div class="layout">

          			<?php if ( get_sub_field( 'content_link_url' ) || get_sub_field( 'content_link_title' ) ) : ?>
            		<div class="layout__item large-and-up-1/2">
              			<a href="<?php the_sub_field( 'content_link_url' ); ?>" class="btn btn--primary btn--white"><?php the_sub_field( 'content_link_title' ); ?></a>
            		</div>
            		<?php endif; ?>

            		<?php if ( get_sub_field( 'stories_page_link' ) ) : ?>
            		<div class="layout__item large-and-up-1/2">
              			<a href="<?php the_sub_field( 'stories_page_link' ); ?>" class="btn btn--primary">View Employer Stories</a>
            		</div>
            		<?php endif; ?>

          		</div>
        	</div><!-- /.text-center -->
      	</div><!-- /.container container--small -->
    </div>
</div>