<?php
/**
 * Flexible sections
 *
 * Template part for rendering ACF flexible sections
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
while ( the_flexible_field( 'advanced_sections' ) ) :

	// START NUMBERED BLOCKS SECTION
	if ( get_row_layout() == 'numbered_blocks' ) :
		get_template_part( 'partials/flexible/advanced', 'numbered-blocks' );
	// END NUMBERED BLOCKS SECTION

	// START OUR PEOPLE SECTION
	elseif ( get_row_layout() == 'our_people' ) :
		get_template_part( 'partials/flexible/advanced', 'our-people' );
	// END OUR PEOPLE SECTION

	// START EASY MAP SECTION
	elseif ( get_row_layout() == 'easy_map' ) :
		get_template_part( 'partials/flexible/advanced', 'easy-map' );
	// END EASY MAP SECTION

	// START MAP TABS SECTION
	elseif ( get_row_layout() == 'map_tabs' ) :
		get_template_part( 'partials/flexible/advanced', 'map-tabs' );
	// END MAP TABS SECTION

	// START MEMBERSHIP TYPES SECTION
	elseif ( get_row_layout() == 'membership_types' ) :
		get_template_part( 'partials/flexible/advanced', 'membership-types' );
	// END MEMBERSHIP TYPES SECTION

	// START FAQ SECTION
	elseif ( get_row_layout() == 'faqs_layout' ) :
		get_template_part( 'partials/flexible/advanced', 'faqs' );
	// END FAQ SECTION
	
	// START PRESS GRID SECTION
	elseif ( get_row_layout() == 'press_grid' ) :
		get_template_part( 'partials/flexible/advanced', 'press-grid' );
	// END PRESS GRID SECTION
	
	// START ICON SLIDER SECTION
	elseif ( get_row_layout() == 'icon_slider' ) :
		get_template_part( 'partials/flexible/advanced', 'icon-slider' );
	// END ICON SLIDER SECTION
	
	// START ICON GRID SECTION
	elseif ( get_row_layout() == 'icon_grid' ) :
		get_template_part( 'partials/flexible/advanced', 'icon-grid' );
	// END ICON GRID SECTION
	
	// START LATEST STORIES SECTION
	elseif ( get_row_layout() == 'latest_stories' ) :
		get_template_part( 'partials/flexible/advanced', 'latest-stories' );
	// END LATEST STORIES SECTION
	
	// START TESTIMONIALS SECTION
	elseif ( get_row_layout() == 'testimonials' ) :
		get_template_part( 'partials/flexible/advanced', 'testimonials' );
	// END TESTIMONIALS SECTION

	// START WHAT WE DO STEPS SECTION
	elseif ( get_row_layout() == 'what_we_do_steps' ) :
		get_template_part( 'partials/flexible/advanced', 'what-we-do-steps' );
	// END WHAT WE DO STEPS SECTION

	// START EMPLOYER PROGRAMME SECTION
	elseif ( get_row_layout() == 'employer_programme' ) :
		get_template_part( 'partials/flexible/advanced', 'employer-programme' );
	// END EMPLOYER PROGRAMME SECTION

	endif; // get_row_layout()

endwhile; // the_flexible_field( 'advanced_sections' )