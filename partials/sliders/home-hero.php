<?php
/**
 * Home Hero
 *
 * Template part for rendering hero section for both, desktop and mobile devices, on home page.
 *
 * @uses hero_slider_item_before()
 * @uses hero_slider_item_after()
 *
 * @see inc/plugins/slider-filters.php
 *
 * @package WordPress
 * @subpackage House Slider
 */
/**
 * Don't bother if we are not on home page
 */
if ( ! is_front_page() ) {
	return;
}
/**
 * Slider for desktop, laptop, tablet
 *
 * @uses ACF 'Home Hero Slider', slider-home-hero.json
 */
/**
 * Add filters for modifying markup
 */
add_filter( 'complex_slider_body_before', 'hero_slider_item_before' );
add_filter( 'complex_slider_body_after', 'hero_slider_item_after' );
/**
 * Call the slider
 */
house_slider_complex( 'home_hero_slider' );
/**
 * Remove filters to prevent modifications effect other sliders
 */
remove_filter( 'complex_slider_body_before', 'hero_slider_item_before' );
remove_filter( 'complex_slider_body_after', 'hero_slider_item_after' );