<?php
/**
 * Template Name: Tools and Resources
 * The template for displaying stories.
 *
 * @package WordPress
 */
get_header(); ?>

<div class="main-content">
	<div class="bgr-gray">
		<div class="container pb++">

		<?php
			/**
			 * Get breadcrumbs
			 */
			get_template_part( 'partials/navigations/breadcrumbs'); ?>

		<h1><?php the_title(); ?></h1>

		<?php
			/**
			 * Get the content
			 */
			while ( have_posts() ) : the_post();
				the_content();
			endwhile; // end of the loop.

			/**
			 * Get resources
			 */
			get_template_part( 'partials/content/resources' );
		?>

		</div><!-- container -->
	</div><!-- bgr-gray -->

	<?php
		/**
		 * Get subscribe section
		 */
		get_template_part( 'partials/content/subscribe' ); ?>

</div><!-- main-content -->

<?php get_footer();