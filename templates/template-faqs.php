<?php
/**
 * Template Name: FAQs
 *
 * Page template for rendering page content with sidebar.
 *
 * @package  WordPress
 */
get_header(); ?>

<main class="left-sidebar clearfix">

	<?php get_template_part( 'partials/navigations/sidebar' ); ?>

	<div class="main-content">

		<div class="container">
			<?php get_template_part( 'partials/navigations/breadcrumbs'); ?>
		</div>

		<?php while ( have_posts() ) : the_post(); ?>
			<section class="mb+">
				<div class="container">
					<h1 class="mb"><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</section>
		<?php endwhile; // end of the loop. ?>

		<?php get_template_part( 'partials/flexible/sections' ); ?>

		<?php get_template_part( 'partials/content/subscribe' ); ?>

	</div>

</main>

<?php get_footer(); ?>