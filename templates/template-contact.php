<?php
/**
 * Template Name: Contact
 *
 * Page template for rendering contact page.
 *
 * @package  WordPress
 */
get_header(); ?>

<main class="left-sidebar clearfix">

	<?php get_template_part( 'partials/navigations/sidebar' ); ?>

	<div class="main-content">

		<div class="container">
			<?php get_template_part( 'partials/navigations/breadcrumbs'); ?>
		</div>

		<?php while ( have_posts() ) : the_post();
				/**
				 * Get flexible sections
				 */
				get_template_part( 'partials/flexible/sections' );

			endwhile; // end of the loop.

			/**
			 * Get contact form 7 template
			 */
			get_template_part( 'partials/forms/contact-form' );
			/**
			 * Get subscribe section
			 */
			get_template_part( 'partials/content/subscribe' );
		?>

	</div>

</main>

<?php get_footer(); ?>