<?php
/**
 * Template Name: Jobs
 *
 * Page template for rendering jobs listings page.
 *
 * @package  WordPress
 */
get_header(); ?>

<main class="left-sidebar clearfix">

	<?php get_template_part( 'partials/navigations/sidebar' ); ?>

	<div class="main-content">

		<div class="container">
			<?php get_template_part( 'partials/navigations/breadcrumbs'); ?>

			<section class="mb+ desktop-and-up-mb++">
				<h1 class="mb- desktop-and-up-mb"><?php the_title(); ?></h1>

				<div class="simple-intro__text">
					<?php
						/**
						 * Get the intro
						 */
						if ( get_field( 'jobs_intro' ) ) :
							echo get_field( 'jobs_intro' ) . '<br>';
						endif; // get_field( 'jobs_intro' )

						/**
						 * Get newsletter intro and link
						 */
						if ( get_field( 'jobs_newsletter_link' ) ) {
							$newsletter = ' <a class="email" href="' . get_field( 'jobs_newsletter_link' ) . '">ebulletin</a>';
						} else {
							$newsletter = '';
						}

						if ( get_field( 'jobs_newsletter_text' ) ) :
							echo get_field( 'jobs_newsletter_text' ) . $newsletter . '.<br>';
						endif; // get_field( 'jobs_newsletter_text' )

						/**
						 * Get twitter intro and link
						 */
						if ( get_field( 'jobs_twitter_username' ) ) {
							$twitter = ' <a target="_blank" href="http://twitter.com/' . get_field( 'jobs_twitter_username' ) . '" target="_blank">@' . get_field( 'jobs_twitter_username' ) . '</a>';

							if ( get_field( 'jobs_twitter_text' ) ) :
								echo get_field( 'jobs_twitter_text' ) . $twitter . '.';
							endif; // get_field( 'jobs_twitter_text' )
						}
					?>
				</div><!-- simple-intro__text -->
			</section>

		</div><!-- container -->

		<?php
			/**
			 * Get jobs posts
			 * @var obj|Error
			 */
			$jobs = get_jobs();

			if ( $jobs->have_posts() ) : ?>
				<section class="bgr-gray pv+ desktop-and-up-pv++">
					<div class="container">

					<?php while ( $jobs->have_posts() ) : $jobs->the_post();
						get_template_part( 'partials/content/job');
					endwhile; // $jobs->have_posts() ?>

					</div><!-- container -->
				</section><!-- bgr-gray pv+ -->
			<?php endif; // $jobs->have_posts()
			wp_reset_query();

		get_template_part( 'partials/content/subscribe' ); ?>

	</div><!-- main-content -->
</main><!-- left-sidebar clearfix -->

<?php get_footer(); ?>