<?php
/**
 * Template Name: Registration
 * The template for displaying form for registering schools.
 *
 * @package WordPress
 */
get_header(); ?>

<div class="main-content school-registration student-reg">

	<div class="container">
		<?php
			/**
			 * Get breadcrumbs
			 */
			get_template_part( 'partials/navigations/breadcrumbs' ); ?>
	</div><!-- container -->

	<?php
		/**
		 * Get flexible fields
		 */
		get_template_part( 'partials/flexible/sections' );

		if ( get_field( 'registration_highlight_content' ) || get_field( 'registration_regular_content' ) ) : ?>

			<div class="container">
				<?php if ( get_field( 'registration_highlight_content' ) ) : ?>
				<div class="school-registration__highlight">
					<p class="highlight"><?php the_field( 'registration_highlight_content' ); ?></p>
				</div>
				<?php endif; // get_field( 'registration_highlight_content' )

				if ( get_field( 'registration_regular_content' ) ) : ?>
					<div class="school-registration__text-box text-box--center">
						<?php the_field( 'registration_regular_content' ); ?>
					</div>
				<?php endif; // get_field( 'registration_regular_content' ) ?>
			</div><!-- container -->

		<?php endif; // get_field( 'registration_highlight_content' ) || get_field( 'registration_regular_content' )

		/**
		 * There was a select field for choosing between registration
		 * forms (school, student etc), but since we're not needing
		 * others than school one, I'll just call the registration template part.
		 */
		get_template_part( 'partials/forms/school-registration' );

	/**
	 * Get subscribe section
	 */
	get_template_part( 'partials/content/subscribe' ); ?>

</div><!-- main-content -->

<?php get_footer();