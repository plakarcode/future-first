<?php
/**
 * Template Name: Home page
 *
 * Page template for home page
 *
 * @package WordPress
 */
get_header();

	/**
	 * Get hero slider for home page
	 */
	get_template_part( 'partials/sliders/home', 'hero' ); ?>

	<div class="main-content">

		<!-- div class="home__search-form p+">
			<?php
				/**
				 * Get search form
				 */
				//get_search_form();
			?>
		</div --><!-- home__search-form p+ -->

	<?php
		/**
		 * Get involved section
		 */
		get_template_part( 'partials/content/get-involved' );
		/**
		 * Get partners section
		 */
		get_template_part( 'partials/content/partners' ); ?>

		<div class="home__about">
			<?php
				/**
				 * Get news section
				 */
				get_template_part( 'partials/content/news' );
				/**
				 * Get story section
				 */
				get_template_part( 'partials/content/home', 'story' );
				/**
				 * Get story section
				 */
				get_template_part( 'partials/content/home', 'about' );
			?>
		</div><!-- home__about -->

	<?php
		/**
		 * Get subscribe section
		 */
		get_template_part( 'partials/content/subscribe' ); ?>

	</div><!-- main-content -->

<?php get_footer();