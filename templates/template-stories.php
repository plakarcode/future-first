<?php
/**
 * Template Name: Stories
 * The template for displaying stories.
 *
 * @package WordPress
 */
get_header(); ?>

<div class="main-content">
	<div class="container">

	<?php
		/**
		 * Get breadcrumbs
		 */
		get_template_part( 'partials/navigations/breadcrumbs' ); ?>

	<h1><?php the_title(); ?></h1>

	<?php
		/**
		 * Get the content
		 */
		while ( have_posts() ) : the_post();
			the_content();
		endwhile; // end of the loop.

		/**
		 * Get the featured image
		 * if one is set
		 */
		if ( function_exists( 'house_featured_image' ) ) {
			/**
			 * Translators: image size, link to post, echo the image
			 */
			house_featured_image( 'full', false, true );
		}
	?>

	</div><!-- container -->

	<div class="bgr-gray pv++ mt++">
		<div class="container">
			<?php get_template_part( 'partials/content/stories' ); ?>
		</div>
	</div>

	<?php
		/**
		 * Get subscribe section
		 */
		get_template_part( 'partials/content/subscribe' ); ?>

</div><!-- main-content -->

<?php get_footer();