<?php
/**
 * Template Name: Legal
 *
 * Page template for rendering legal pages
 *
 * @package  WordPress
 */
get_header(); ?>

<main class="left-sidebar clearfix">

	<?php get_template_part( 'partials/navigations/legal' ); ?>

	<div class="main-content">

		<div class="container">
			<?php get_template_part( 'partials/navigations/breadcrumbs'); ?>
		</div><!-- container -->

		<?php while ( have_posts() ) : the_post(); ?>
			<section class="typography">
				<div class="container">
					<h1><?php the_title(); ?></h1>

					<?php
						/**
						 * Get legal flexible fields
						 */
						while ( the_flexible_field( 'legal_content' ) ) :

							// START HEADING SECTION
							if ( get_row_layout() == 'heading_layout' ) : ?>
								<h2><?php the_sub_field( 'heading' ); ?></h2>
							<?php // END HEADING SECTION

							// START FEATURED PARAGRAPH SECTION
							elseif ( get_row_layout() == 'featured_layout' ) : ?>
							<p class="featured"><?php the_sub_field( 'featured' ); ?></p>
							<?php // END FEATURED PARAGRAPH SECTION

							// START REGULAR CONTENT SECTION
							elseif ( get_row_layout() == 'content_layout' ) :
								the_sub_field( 'content' );
							// END REGULAR CONTENT SECTION

							// START LIST SECTION
							elseif ( get_row_layout() == 'list_layout' ) :
								if ( have_rows( 'list' ) ) : ?>
									<ul>
										<?php while ( have_rows( 'list' ) ) : the_row( 'list' ); ?>
											<li><?php the_sub_field( 'item' ); ?></li>
										<?php endwhile; // have_rows( 'list' ) ?>
									</ul>
								<?php endif; // have_rows( 'list' )
							// END LIST SECTION

							endif; // get_row_layout()

						endwhile; // the_flexible_field( 'legal_content' )
					?>
				</div><!-- container -->
			</section><!-- typography -->

		<?php endwhile; // end of the loop. ?>

	</div><!-- main-content -->
</main><!-- left-sidebar clearfix -->

<?php get_footer();