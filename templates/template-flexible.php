<?php
/**
 * Template Name: Flexible Full Width
 *
 * The template for displaying flexible sections in full width.
 *
 * @package WordPress
 */
get_header(); ?>

	<div class="main-content">

		<div class="container">
			<?php get_template_part( 'partials/navigations/breadcrumbs'); ?>
		</div>

		<?php
			/**
			 * Get flexible sections
			 */
			get_template_part( 'partials/flexible/sections' );

			/**
			 * Get subscribe section
			 */
			get_template_part( 'partials/content/subscribe' );
		?>

	</div><!-- main-content -->

<?php get_footer();