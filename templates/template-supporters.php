<?php
/**
 * Template Name: Employers and Supporters
 *
 * Page template for rendering page content with sidebar.
 *
 * @package  WordPress
 */
get_header(); ?>

<main class="left-sidebar clearfix">

	<?php get_template_part( 'partials/navigations/sidebar' ); ?>

	<div class="main-content">

		<div class="container">
			<?php get_template_part( 'partials/navigations/breadcrumbs'); ?>
		</div>

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'partials/flexible/sections' ); ?>

		<?php endwhile; // end of the loop. ?>

		<div class="container">
			<?php get_template_part( 'partials/content/employers-supporters' ); ?>
		</div>

		<?php get_template_part( 'partials/content/subscribe' ); ?>

	</div>

</main>

<?php get_footer(); ?>