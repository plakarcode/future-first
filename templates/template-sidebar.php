<?php
/**
 * Template Name: Page with sidebar
 *
 * Page template for rendering page content with sidebar.
 *
 * @package  WordPress
 */
get_header(); ?>

<main class="left-sidebar clearfix">

	<?php get_template_part( 'partials/navigations/sidebar' ); ?>

	<div class="main-content">

		<div class="container">
			<?php get_template_part( 'partials/navigations/breadcrumbs'); ?>
		</div>

		<?php get_template_part( 'partials/flexible/sections' ); ?>

		<?php get_template_part( 'partials/content/subscribe' ); ?>

	</div><!-- main-content -->
</main><!-- left-sidebar clearfix -->

<?php get_footer(); ?>