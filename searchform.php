<?php
/**
 * The template for displaying search form
 *
 * @package WordPress
 */
?>
<div class="layout">
	<form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url( '/' ); ?>">
		<div class="layout__item large-and-up-5/6 medium-2/3">
			<label class="screen-reader-text" for="s"><?php _e( 'Search for:', 'house' ); ?></label>
			<input type="text" value="" name="s" id="s" class="input  input--primary input--search" placeholder="<?php esc_attr_e( 'Find your old school or college', 'house' ); ?>"  />
		</div><!-- layout__item large-and-up-5/6 medium-2/3 -->
		<div class="layout__item large-and-up-1/6 medium-1/3">
			<input type="submit" id="searchsubmit" class="btn btn--primary btn--pink btn--full" value="<?php esc_attr_e( 'Search', 'house' ); ?>" />
		</div><!-- layout__item large-and-up-1/6 medium-1/3 -->
	</form>
</div><!-- layout -->