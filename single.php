<?php
/**
 * Single
 *
 * The Template for displaying all single posts.
 *
 * @package WordPress
 */
get_header(); ?>

<div class="main-content">

	<?php while ( have_posts() ) : the_post(); ?>

		<div class="container">
			<?php get_template_part( 'partials/navigations/breadcrumbs'); ?>

			<div class="news-inner__heading">
				<?php if ( in_category( 'blog' ) ) : ?>
					<p class="date type-uppercase"><?php echo get_the_date( 'd F Y' ); ?></p>
				<?php endif; ?>
				<h1><?php the_title(); ?></h1>

				<div class="layout">
					<div class="layout__item 1/2">
						<?php if ( in_category( 'blog' ) ) : ?>
							<p class="type-uppercase">by <?php the_author(); ?></p>
						<?php endif; ?>
					</div><!-- layout__item 1/2 -->

					<div class="layout__item 1/2 text-right">
						<ul class="list-inline">
							<?php if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
								<li><?php echo house_image( 'news/comment-icon.png' ); ?></li>
								<li><?php comments_popup_link( '0', '1', '%', 'comments-link', 'Comments are closed' ); ?></li>
							<?php endif; // ! post_password_required() && ( comments_open() || get_comments_number() ) ?>
						</ul>
					</div><!-- layout__item 1/2 text-right -->
				</div>
			</div><!-- news-inner__heading -->
		</div><!-- container -->

		<?php
			/**
			 * If we have template part for post format and
			 * we are on post format single.
			 *
			 * WordPress will first look for 'format-FORMAT_NAME.php',
			 * if none found fallback is 'format.php'. If that one is missing as well,
			 * it'll look for 'content.php'
			 */
			if ( has_post_format( get_post_format() ) ) {
				get_template_part( 'format', get_post_format() );
			}
			/**
			 * If we have template part for custom post type and
			 * we are on custom post type single.
			 *
			 * WordPress will first look for 'content-CPT_NAME.php',
			 * if none found fallback is 'content.php'.
			 */
			elseif ( post_type_exists( get_post_type() ) ) {
				get_template_part( 'content', get_post_type() );
			}
			/**
			 * If, in any case, none from above applies
			 */
			else {
				get_template_part( 'content' );
			}

			// related news

	endwhile; // end of the loop.

	/**
	 * Get related news
	 */
	get_template_part( 'partials/content/related-news' );
	/**
	 * Get subscribe section
	 */
	get_template_part( 'partials/content/subscribe' ); ?>

</div><!-- main-content -->

<?php get_footer();