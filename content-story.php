<?php
/**
 * The default template for displaying content of story.
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="container">
		<div class="story-main">

			<?php the_post_thumbnail(); ?>

			<?php
				/**
				 * Get flexible content fields if any,
				 * otherwise get regular content
				 */
				if ( get_field( 'story_popup_content' ) ) :

					get_template_part( 'partials/content/story-sections' );

				endif; // get_field( 'content_fields' )
			?>

			<div class="justifize mt+">
				<div class="justifize__box">
					<?php get_template_part( 'partials/content/share' ); ?>
				</div><!-- justifize__box -->
			</div><!-- justifize mt+ -->

		</div><!-- story-main -->
	</div><!-- container -->
</article><!-- #post -->