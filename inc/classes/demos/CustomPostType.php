<?php
/**
 * How to use CustomPostType class
 *
 * @package WordPress
 */

/**
 * Register custom post type
 * @var CustomPostType
 */
$cpt = new CustomPostType( 'cpt' );

/**
 * Set menu icon for custom post type
 *
 * @link https://developer.wordpress.org/resource/dashicons/
 */
$cpt->menu_icon( 'dashicons-carrot' );

/**
 * Register custom post type with custom options
 * @var array
 */
$options = array(
	'public' => true,
	'menu_position' => 5,
	'supports' => array(
		'title',
		'editor',
		'author',
		'thumbnail'
	)
);

$cpt = new CustomPostType( 'cpt', $options );

/**
 * Custom taxonomies
 * @var string
 */
$taxonomy = 'custom-taxonomy';

/**
 * Register custom taxonomy, hierarchical by default.
 */
$cpt->register_taxonomy( $taxonomy );

/**
 * Make custom taxonomy non hierarchical
 */
$cpt->taxonomy_settings[$taxonomy]['hierarchical'] = 0;

/**
 * Register existing taxonomy with custom post type,
 * except 'post_formats'
 *
 * @var array
 */
$cpt->exisiting_taxonomies = array( 'post_tag', 'category' );