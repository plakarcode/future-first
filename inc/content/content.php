<?php
/**
 * Custom modifications of the the_content()
 *
 * @package WordPress
 */

/**
 * Content trim characters
 *
 * Trim content down to exact number of characters.
 *
 * @uses trim_string() @see /inc/general.php
 *
 * @param  int $length 		Number of characters
 * @param  string $after  	Append to content
 * @return string         	Returns trimmed content string
 */
function content_trim_characters( $length, $after = '...' ) {
	$str = apply_filters( 'the_content', get_the_content() );

	if ( ! ( strlen( $str ) <= $length ) ) {
		$str = trim_string( $str, $length, $after );
	}

	return $str;
}
/**
 * Content trim words
 *
 * Trim content down to exact number of words.
 *
 * @uses trim_string() @see /inc/general.php
 *
 * @param  int $length 		Number of words
 * @param  string $after  	Append to content
 * @return string         	Returns trimmed content string
 */
function content_trim_words( $length, $after = '...' ) {
	$str = apply_filters( 'the_content', get_the_content() );

	if ( ! ( strlen( $str ) <= $length ) ) {
		$str = wp_trim_words( $str, $length, $after );
	}

	return $str;
}
/**
 * Check if content is really empty
 *
 * @link http://blog.room34.com/archives/5360
 *
 * @param  string $str   Post content
 * @return bool          True or false on content being empty
 */
function empty_content( $str ) {
    return trim( str_replace( '&nbsp;', '', strip_tags( $str ) ) ) == '';
}
/**
 * Highlight Search results
 *
 * Filter through posts and pages titles and excerpts and highlight
 * search keyword on search results page.
 *
 * This function is attached to 'the_excerpt', 'the_content' and 'the_title' filters hooks.
 *
 * @param  string $content Content for filtering
 * @return string       Returns filtered string
 */
function highlight_search_results( $content ) {

	if ( is_search() && ! is_admin() && ! is_post_type_archive( 'centre' ) ) {
		$sr = get_query_var( 's' );
		$keys = explode( " ", $sr );
		$keys = array_filter( $keys );
		$content = preg_replace( '/(' . implode( '|', $keys ) . ')/iu', '<span class="s-highlight">\0</span>', $content );
	}

	return $content;
}
// add_filter( 'the_excerpt', 'highlight_search_results' );
// add_filter( 'the_content', 'highlight_search_results' );
// add_filter( 'the_title', 'highlight_search_results' );

/**
 * Highlight custom search results
 *
 * We have custom query for search results which makes default filters not working completely.
 * @see highlight_search_results()
 *
 * So we are creating custom filtering for any passed string.
 *
 * @param  string $string Content to be filtered through
 * @return string         Returnes filtered content
 */
function highlight_custom_search( $string ) {
	$highlight = $string;
	$keys = implode( '|', explode( ' ', get_search_query() ) );
	$highlight = preg_replace( '/(' . $keys .')/iu', '<span class="s-highlight">\0</span>', $highlight );

	return $highlight;
}
