<?php
/**
 * Get the like count
 * 
 * @global type $wpdb
 * @param type $post_id
 * @return type
 */
function get_likes_number( $post_id ) {
	global $wpdb;
	
	$post_id = (int) $post_id;
	
	$sql = "select count(*) as count from wp_wti_like_post where post_id = " . $post_id;
	$results = current( $wpdb->get_results( $sql ) );
	
	return $results->count;
}
