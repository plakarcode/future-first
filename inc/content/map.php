<?php
/**
 * Map functions
 *
 * Get Map
 *
 * @package WordPress
 */

/**
 * Get Staff Sorted By Type
 *
 * @return Array of maps and markers
 */
function get_maps() {

	$maps = [];
	$map_counter = 0;

	// try loop through flexible fields
	while ( the_flexible_field( 'content_fields' ) ) :

		// if we have advanced fields
		if ( get_row_layout() == 'advanced_sections_layout' ) :

			while ( the_flexible_field( 'advanced_sections' ) ) :

				if ( get_row_layout() == 'map_tabs' ) :

					$tabs = get_sub_field( 'tabs' );

					if ( $tabs ) :

						foreach ( $tabs as $tab ) :

							$map_counter++;

							$row = get_row();
							$canvas_id = 'map_canvas_' . $map_counter;

							if ( $tab['height'] ) {
								$height = $tab['height'];
							} else {
								$height = '585';
							}

							$maps[$canvas_id] = [
								'canvas_id'			=> $canvas_id,
								'centre_latitude'	=> $tab['centre_latitude'],
								'centre_longitude'	=> $tab['centre_longitude'],
								'marker_image'		=> $tab['marker_image'],
								'zoom'				=> $tab['zoom'],
								'height'			=> $height,
								'markers'			=> $tab['markers'],
							];

						endforeach;

					endif; // $tabs

				endif; // get_row_layout() == 'map'

			endwhile ; // the_flexible_field( 'advanced_sections' )

		endif; // get_row_layout() == 'advanced_sections_layout'

	endwhile; // the_flexible_field( 'content_fields' )

	return $maps;
}