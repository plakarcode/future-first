<?php
/**
 * Custom queries
 *
 * @package WordPress
 */
/**
 * Modify default buzz plugin query args
 *
 * This function is attached to 'buzz_posts_query' filter hook.
 * @see  index.php
 *
 * @param  array $args Array of query args
 * @return array       Returnes filtered query args
 */
function modify_query_paged( $args ) {
	global $globalSite;
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	$all = get_all_posts_ids( 'twitter_data_wrap', 'option' );

	$args['post__in'] = $all;
	$args['paged'] = $paged;
	$args['posts_per_page'] = $globalSite['posts_per_page'];

	return $args;
}