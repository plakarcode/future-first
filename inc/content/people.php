<?php
/**
 * People functions
 *
 * Get People
 *
 * @package WordPress
 */

/**
 * Get People Sorted By Type
 * 
 * @return Array of people
 */
function get_people() {
	
	$people = get_field( 'people', 'option' );

	$array = [];
	
	foreach ($people as $member) {
		$array[ $member[ 'type' ] ][] = $member;
	}
	
	return $array;
}

