<?php
/**
 * Here include all content related custom functions
 */

// excerpts
include( get_template_directory() . '/inc/content/excerpts.php' );
// content
include( get_template_directory() . '/inc/content/content.php' );
// footer copyrights
include( get_template_directory() . '/inc/content/footer-copyrights.php' );
// body and article classes
include( get_template_directory() . '/inc/content/content-classes.php' );
// comments callbacks
include( get_template_directory() . '/inc/content/comments.php' );
// dashboard filters for post formats
include( get_template_directory() . '/inc/content/filter-post_format.php' );
// people
include( get_template_directory() . '/inc/content/people.php' );
// map
include( get_template_directory() . '/inc/content/map.php' );
// likes
include( get_template_directory() . '/inc/content/likes.php' );
// custom queries
include( get_template_directory() . '/inc/content/custom-queries.php' );
/**
 * Ajax loading posts functionality
 */
include( get_template_directory() . '/inc/content/ajax/index.php' );