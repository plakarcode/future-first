<?php
/**
 * Contact Form 7 customizations
 *
 * Create all default customizations here. Contact Form 7 lacks dev's documentation
 * in both places, website and code. However, there are some helpful links out there,
 * plus reading the plugin' code itself can make workarounds.
 *
 * @link https://github.com/jgrossi/contact-form-7-hooks
 * @link http://xaviesteve.com/3298/wordpress-contact-form-7-hook-unofficial-developer-documentation-and-examples/
 * @link https://plugins.trac.wordpress.org/browser/contact-form-7-accessible-defaults/trunk/contact-form-7-accessible-form.php
 * @link http://contactform7.com/2015/03/28/custom-validation/
 * @link http://code.tutsplus.com/tutorials/mini-guide-to-contact-form-7--wp-25086
 *
 * @package WordPress
 * @subpackage Contact Form 7
 */
/**
 * Registration school form template
 *
 * Create registration school form template. Default template can be found in plugin's
 * files 'wp-content/plugins/contact-form-7/includes/contact-form-template.php'
 *
 * @see WPCF7_ContactFormTemplate::form()
 *
 * @return string Returns form markup
 */
function registration_school_form_template() {
	/**
	 * Set school name
	 * @var string
	 */
	$school_name = '[text* school-name id:school-name class:input class:input--primary placeholder "School Name*"]';
	/**
	 * Set address
	 * @var string
	 */
	$address = '[text* address id:address class:input class:input--primary placeholder "Address*"]';
	/**
	 * Set name
	 * @var string
	 */
	$name = '[text* contact-name id:contact-name class:input class:input--primary placeholder "Contact Name*"]';
	/**
	 * Set email
	 * @var string
	 */
	$email = '[email* contact-email id:contact-email class:input class:input--primary placeholder "Contact Email*"]';
	/**
	 * Set phone
	 * @var string
	 */
	$phone = '[tel* contact-phone id:contact-phone class:input class:input--primary placeholder "Phone Number*"]';
	/**
	 * Set job
	 * @var string
	 */
	$job = '[text contact-job id:contact-job class:input class:input--primary placeholder "Contact Job Title"]';
	/**
	 * Set textarea
	 * @var string
	 */
	$message = '[textarea contact-message class:input class:input--primary class:input--textarea placeholder "Message"]';
	/**
	 * Set submit button
	 * @var string
	 */
	$submit = '[submit class:btn class:btn--primary class:btn--medium "Send message"]';
	/**
	 * Anti-spam field
	 * Hidden field that should stay empty if user is human
	 * @uses Contact Form 7 Honeypot plugin
	 */
	$spam = '[honeypot school-website]';

	$form = '<div class="layout">';
	$form .= '<div class="layout__item medium-and-up-1/2">' . $school_name . '</div>';
	$form .= '<div class="layout__item medium-and-up-1/2">' . $address . '</div>';
	// Throw in hidden field is Honeypot plugin is active.
	// Wrapping into <div> will prevent CF7 to wrap field into <p>
	// and apply margins and other unwanted styling
	if ( house_is_plugin_active( 'contact-form-7-honeypot/honeypot.php' ) ) {
		$form .= '<div>' . $spam . '</div>';
	}
	$form .= '<div class="layout__item medium-and-up-1/2 desktop-and-up-mt-">' . $name . '</div>';
	$form .= '<div class="layout__item medium-and-up-1/2 desktop-and-up-mt-">' . $email . '</div>';
	$form .= '<div class="layout__item medium-and-up-1/2 desktop-and-up-mt-">' . $phone . '</div>';
	$form .= '<div class="layout__item medium-and-up-1/2 desktop-and-up-mt-">' . $job . '</div>';
	$form .= '<div class="layout__item desktop-and-up-mt-">' . $message . '</div>';
	$form .= '<div class="layout__item text-center mt- desktop-and-up-mt">' . $submit . '</div>';
	$form .= '</div>';

	return $form;
}
/**
 * Registration school sender email
 *
 * Set the 'from' email address to be used in email template. Default template can be found in plugin's
 * files 'wp-content/plugins/contact-form-7/includes/contact-form-template.php'
 *
 * @see WPCF7_ContactFormTemplate::from_email()
 *
 * @return string Returns 'from' email address
 */
function registration_school_sender_email() {
	$admin_email = get_option( 'admin_email' );
	$sitename = strtolower( $_SERVER['SERVER_NAME'] );

	if ( wpcf7_is_localhost() ) {
		return $admin_email;
	}

	if ( substr( $sitename, 0, 4 ) == 'www.' ) {
		$sitename = substr( $sitename, 4 );
	}

	if ( strpbrk( $admin_email, '@' ) == '@' . $sitename ) {
		return $admin_email;
	}

	return 'school-registration@' . $sitename;
}
/**
 * Registration school mail to client
 *
 * Settings for custom email template that will be sent to client
 * as direct message from site visitor. Default template can be found in plugin's
 * files 'wp-content/plugins/contact-form-7/includes/contact-form-template.php'
 *
 * @see WPCF7_ContactFormTemplate::mail()
 *
 * @link http://contactform7.com/special-mail-tags/
 * @return array Returns array of email settings
 */
function registration_school_email_to_client() {
	/**
	 * Set 'to'
	 * @var string
	 */
	$recipient = get_option( 'admin_email' );
	/**
	 * Set 'from'
	 * @var string
	 */
	$sender = sprintf( '%1$s <%2$s>', get_bloginfo( 'name' ), registration_school_sender_email() );
	/**
	 * Set subject
	 * @var string
	 */
	$subject = sprintf(	_x( 'Message from %2$s to %1$s ', 'mail subject', 'house' ),
		get_bloginfo( 'name' ),
		'[contact-name]'
	);
	/**
	 * Set additional headers
	 * @var string
	 */
	$additional_headers = 'Reply-To: [contact-email]';
	/**
	 * Set email body
	 * @var string
	 */
	$body = sprintf( __( 'You have received new message on %s.', 'house' ), get_bloginfo( 'name' ) ) . "\n\n"
			. __( 'Sender has provided following info:', 'house' ) . "\n\n"
			. sprintf( __( 'School Name: %s', 'house' ), '[school-name]' ) . "\n"
			. sprintf( __( 'Address: %s', 'house' ), '[address]' ) . "\n"
			. sprintf( __( 'Name: %s', 'house' ), '[contact-name]' ) . "\n"
			. sprintf( __( 'Email: %s', 'house' ), '[contact-email]' ) . "\n"
			. sprintf( __( 'Phone: %s', 'house' ), '[contact-phone]' ) . "\n"
			. sprintf( __( 'Job: %s', 'house' ), '[contact-job]' ) . "\n"
			. __( 'Message:', 'house' ) . "\n"
			. '[contact-message]' . "\n\n\n"
			. sprintf( __( 'This e-mail was sent from %1$s, on %2$s at %3$s.', 'house' ),
				'[_url]',
				'[_date]',
				'[_time]'
			);

	$template = array(
		'subject'            => $subject,
		'sender'             => $sender,
		'body'               => $body,
		'recipient'          => $recipient,
		'additional_headers' => $additional_headers,
		'attachments'        => '',
		'use_html'           => 0,
		'exclude_blank'      => 1
	);

	return $template;
}
/**
 * Registration school Autoresponder
 *
 * Settings for custom email template that will be sent to site visitor
 * as autoresponder. Default template can be found in plugin's files
 * 'wp-content/plugins/contact-form-7/includes/contact-form-template.php'
 *
 * @see WPCF7_ContactFormTemplate::mail_2()
 *
 * @return array Returns array of email settings
 */
function registration_school_autoresponder() {
	/**
	 * Set 'to'
	 * @var string
	 */
	$recipient = '[contact-email]';
	/**
	 * Set 'from'
	 * @var string
	 */
	$sender = sprintf( '%1$s <%2$s>', get_bloginfo( 'name' ), registration_school_sender_email() );
	/**
	 * Set subject
	 * @var string
	 */
	$subject = sprintf(	_x( 'Your message to %s has been sent', 'mail subject', 'house' ),
		get_bloginfo( 'name' )
	);
	/**
	 * Set additional headers
	 * @var string
	 */
	$additional_headers = sprintf( 'Reply-To: %s', get_option( 'admin_email' ) );
	/**
	 * Set email body
	 * @var string
	 */
	$body = sprintf( __( 'Hello %s,', 'house' ), '[contact-name]' ) . "\n\n"

			. sprintf( __( 'Your message to %s has been sent.', 'house' ), get_bloginfo( 'name' ) )
			. __( 'Please, do not reply to this email. We will get back to you as soon as possible.', 'house' ) . "\n\n"

			. __( 'You have provided following info:', 'house' ) . "\n\n"
			. sprintf( __( 'School Name: %s', 'house' ), '[school-name]' ) . "\n"
			. sprintf( __( 'Address: %s', 'house' ), '[address]' ) . "\n"
			. sprintf( __( 'Name: %s', 'house' ), '[contact-name]' ) . "\n"
			. sprintf( __( 'Email: %s', 'house' ), '[contact-email]' ) . "\n"
			. sprintf( __( 'Phone: %s', 'house' ), '[contact-phone]' ) . "\n"
			. sprintf( __( 'Job: %s', 'house' ), '[contact-job]' ) . "\n"
			. __( 'Message:', 'house' ) . "\n"
			. '[contact-message]' . "\n\n\n"

			. sprintf( __( 'This e-mail was sent from %1$s, on %2$s at %3$s.', 'house' ),
				'[_url]',
				'[_date]',
				'[_time]'
			);

	$template = array(
		'active'             => true,
		'subject'            => $subject,
		'sender'             => $sender,
		'body'               => $body,
		'recipient'          => $recipient,
		'additional_headers' => $additional_headers,
		'attachments'        => '',
		'use_html'           => 0,
		'exclude_blank'      => 1
	);

	return $template;
}
