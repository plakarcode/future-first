<?php
/**
 * Filters for House Slider plugin
 *
 * @package WordPress
 * @subpackage House Slider
 */

/**
 * Filters for modifying hero home slider markup
 *
 * We are adding custom content before and after .slide-item__body div. Filters are
 * added and removed right before and after the actuall function call so that modifications
 * don't effect other sliders on website.
 *
 * @see partials/sliders/home-hero.php
 * @return string Returns added markup
 */
function hero_slider_item_before() {
	$before = '<div class="container">
				<div class="tableize tableize--middle tableize--full">
					<div class="tableize__cell">
						<div class="home-banner-text">';
	return $before;
}
function hero_slider_item_after() {
	$after = '</div><!-- home-banner-text -->
			</div><!-- tableize__cell -->
		</div><!-- tableize tableize--middle tableize--full -->

		<div class="scroll-down">
			<a href="#get-involved">
            	<span>scroll down</span>' . house_svg_icon( 'scroll-down' ) . '
          </a>
		</div><!-- end of .scroll-down -->

	</div><!-- container -->';
	return $after;
}

/**
 * Add image caption to news image slider
 *
 * This function is attached to 'simple_slider_img' filter hook.
 *
 * Used in flexible content for single news posts.
 * @see partials/simple-flexible/section-image-slider.php
 *
 * @param string $img Slider image markup
 * @return string  Returns filtered markup
 */
function add_post_slider_img_caption( $img ) {
	$markup = $img;

	if ( get_sub_field( 'slider_text' ) ) :
		$markup .= '<p class="img-sub">' . get_sub_field( 'slider_text' ) . '</p>';
	endif; // get_sub_field( 'slider_text' )

	return $markup;
}
/**
 * Change post slider id
 *
 * This function is attached to 'simple_slider_id' filter hook.
 * @see partials/simple-flexible/section-image-slider.php
 *
 * @param  string $id Slider ID
 * @return string     Modified slider id
 */
function change_post_slider_id( $id ) {
	$id = 'img-slider';
	return $id;
}
/**
 * Change post slider item class
 *
 * This function is attached to 'simple_slider_item_class' filter hook.
 * @see partials/simple-flexible/section-image-slider.php
 *
 * @param  string $class Slider item class
 * @return string        Modified slider item class
 */
function change_post_slider_item_class( $class ) {
	$class = 'item';
	return $class;
}
/**
 * Change page slider id
 *
 * This function is attached to 'simple_slider_id' filter hook.
 * @see partials/flexible/section-image-slider.php
 *
 * @param  string $id Slider ID
 * @return string     Modified slider id
 */
function change_page_slider_id( $id ) {
	$id = 'image-slider';
	return $id;
}
/**
 * Change page slider class
 *
 * This function is attached to 'simple_slider_class' filter hook.
 * @see partials/flexible/section-image-slider.php
 *
 * @param  string $class Slider class
 * @return string     Modified slider class
 */
function change_page_slider_class( $class ) {
	$class = 'image-slider mb+';
	return $class;
}
/**
 * Change page slider item class
 *
 * This function is attached to 'simple_slider_item_class' filter hook.
 * @see partials/flexible/section-image-slider.php
 *
 * @param  string $class Slider item class
 * @return string        Modified slider item class
 */
function change_page_slider_item_class( $class ) {
	$class = 'item';
	return $class;
}