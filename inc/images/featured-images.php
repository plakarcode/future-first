<?php
/**
 * Featured images
 *
 * Get default and theme defined image sizes for featured images.
 *
 * @package WordPress
 */
/**
 * House featured image
 *
 * Get post/page featured image. Optional set images size and
 * whether the image should link to post.
 * @link https://developer.wordpress.org/reference/functions/the_post_thumbnail/
 *
 * Uses Justin Tadlock's 'Get The Image' plugin.
 * @see inc/images/get-the-image.php
 *
 * @param  string  $size   Image size id, 'thumbnail', 'medium', 'large' etc
 * @param  boolean $link   Shoud image link to post or be just image
 * @param  boolean $echo   Return/echo the image
 * @param  string  $class  Image class
 * @return string          Returns/echoes image markup
 */
function house_featured_image( $size = 'full', $link = true, $echo = true, $class = '' ) {
	$args = array(
		/* Post the image is associated with. */
		'post_id'            => get_the_ID(),

		/* Methods of getting an image (in order). */
		'meta_key'           => array( $size ), // array|string
		'featured'           => true,
		'attachment'         => false, // if no thumbnail then it gets first attached image
		'scan'               => false,
		'callback'           => null,
		'default'            => false,

		/* Attachment-specific arguments. */
		'size'               => isset( $_wp_additional_image_sizes[$size] ) ? $size : $size,

		/* Format/display of image. */
		'link_to_post'       => $link, // just image or image in link
		'image_class'        => $class,
		'width'              => false,
		'height'             => false,
		'before'             => '',
		'after'              => '',

		/* Captions. */
		'caption'            => false, // Default WP [caption] requires a width.

		/* Return/echo image. */
		'echo'               => $echo,
	);

	return get_the_image( $args );
}
