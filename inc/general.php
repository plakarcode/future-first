<?php
/**
 * Custom functions not related to anything specific
 */

/**
 * Trim string
 *
 * When we need limited number of characters for a string (i.e excerpt )
 *
 * @link http://stackoverflow.com/questions/12423407/limiting-characters-retrived-from-a-database-field#answer-12423453
 *
 * @param  string 	$str    		String to be trimmed
 * @param  integer 	$length 		Number of characters
 * @return string         			Trimmed string
 */
function trim_string( $str, $length, $after = '...' ) {

	if ( ! ( strlen( $str ) <= $length ) ) {
		$str = substr( $str, 0, strpos( $str, ' ', $length ) ) . ' ' . $after;
	}

	return $str;
}

/**
 * Admin menu Chrome fix
 *
 * Dashboard menu gets broken on hover in Chrome.
 *
 * @link https://core.trac.wordpress.org/ticket/33199
 * @link http://wordpress.stackexchange.com/questions/200096/admin-sidebar-items-overlapping-in-admin-panel
 */
add_action( 'admin_enqueue_scripts', 'chrome_fix' );

function chrome_fix() {

	if ( strpos( $_SERVER[ 'HTTP_USER_AGENT' ], 'Chrome' ) !== false ) {
		wp_add_inline_style( 'wp-admin', '#adminmenu { transform: translateZ(0) }' );
	}
}
/**
 * Is child page
 *
 * Check if current page is child of passed parent.
 *
 * @link http://www.kevinleary.net/wordpress-is_child-for-advanced-navigation/
 * @param  string  $parent   Parent ID, slug or title
 * @return boolean           Returns true or false
 */
function is_child_page( $parent = '' ) {
	global $post;

	$get_parent   = get_page( $post->post_parent, ARRAY_A );
	$parent       = (string) $parent;
	$parent_array = (array) $parent;

	if ( in_array( (string) $get_parent['ID'], $parent_array ) ) {
		return true;
	} elseif ( in_array( (string) $get_parent['post_title'], $parent_array ) ) {
		return true;
	} elseif ( in_array( (string) $get_parent['post_name'], $parent_array ) ) {
		return true;
	} else {
		return false;
	}
}
/**
 * Define irregular plurals
 *
 * @return array Array of irregular plurals
 */
function irregular_plurals() {

	$irregulars = array(
		'man'           => 'men',
		'woman'         => 'women',
		'fungus'        => 'fungi',
		'thief'         => 'thieves',
		'medium'        => 'media',
		'person'        => 'people',
		'echo'          => 'echoes',
		'hero'          => 'heroes',
		'potato'        => 'potatoes',
		'veto'          => 'vetoes',
		'auto'          => 'autos',
		'memo'          => 'memos',
		'pimento'       => 'pimentos',
		'pro'           => 'pros',
		'knife'         => 'knives',
		'leaf'          => 'leaves',
		'bus'           => 'busses',
		'child'         => 'children',
		'quiz'          => 'quizzes',
		# words whose singular and plural forms are the same
		'equipment'     => 'equipment',
		'fish'          => 'fish',
		'information'   => 'information',
		'money'         => 'money',
		'moose'         => 'moose',
		'news'          => 'news',
		'rice'          => 'rice',
		'series'        => 'series',
		'sheep'         => 'sheep',
		'species'       => 'species',
		'press'			=> 'press',
	);

	return $irregulars;
}

/**
 * Pluralize string
 *
 * Check if string is irregular and, relevant to its ending,
 * create plural form.
 *
 * @param  string $string Singular that needs to be pluralized
 * @return string         Returns pluralized sting
 */
function house_pluralize( $string ) {

	$irregulars = irregular_plurals();

	$es = array( 's', 'z', 'ch', 'sh', 'x' );

	$last_letter = substr( $string, -1 );

	if ( array_key_exists( $string, $irregulars ) ) {

		return $irregulars[$string];

	} elseif ( $last_letter == 'y' ) {

		if ( substr( $string, -2 ) == 'ey' ) {

			return substr_replace( $string, 'ies', -2, 2 );

		} else {

			return substr_replace( $string, 'ies', -1, 1 );

		}

	} elseif ( in_array( substr( $string, -2 ), $es ) || in_array( substr( $string, -1 ), $es ) ) {

		return $string . 'es';

	} else {

		return $string . 's';

	}
}