<?php
/**
 * Social functions
 *
 * Custom functionality for social networking
 *
 * @todo build list from all social profile links
 *
 * @package WordPress
 */

/**
 * Get the sharing link
 *
 * Helper function for share links
 *
 * @param  string $network   Network to share to, will be used for svg icon as well
 * @param  string $link      Considering this is one pager, link is just id of element we want to link to
 * @param  string $text      Some networks allow extra text (Twitter)
 * @param  string $hashtags  Hashatgs (Twitter)
 * @return string            Returns share link markup
 */
function get_share_link( $network = '', $link = '', $text = '', $hashtags = '' ) {

	if ( $network == 'twitter' ) {
		$href = 'http://twitter.com/share?text=' . $text . '&url=' . $link . '&hashtags=' . $hashtags;
	} elseif ( $network == 'facebook' ) {
		$href = 'http://www.facebook.com/sharer/sharer.php?u=' . $link;
	} elseif ( $network == 'google' ) {
		$href = 'https://plus.google.com/share?url=' . $link;
	}

	$output = '<a href="' . $href . '"';

	$output .= ' onclick="javascript:window.open(this.href, \'\',\'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=450,centerscreen=yes\');return false;"';

	$output .= '>';
	$output .= house_svg_icon( $network );
	$output .= ' ' . ucwords( $network );

	$output .= '</a>';

	return $output;
}
/**
 * Share link
 *
 * Echo share link
 *
 * @param  string $network   Network to share to, will be used for svg icon as well
 * @param  string $link      Considering this is one pager, link is just id of element we want to link to
 * @param  string $text      Some networks allow extra text (Twitter)
 * @param  string $hashtags  Hashatgs (Twitter)
 * @return string            Echoes share link markup
 */
function share_link( $network = '', $link = '', $text = '', $hashtags = '' ) {
	echo get_share_link( $network, $link, $text, $hashtags );
}

