<?php
/**
 * Here include all custom post type related functions
 */
// jobs cpt
include( get_template_directory() . '/inc/cpt/jobs.php' );
// press cpt
include( get_template_directory() . '/inc/cpt/press.php' );
// story cpt
include( get_template_directory() . '/inc/cpt/story.php' );
// resource cpt
include( get_template_directory() . '/inc/cpt/resource.php' );
