<?php
/**
 * Story custom post type
 *
 * Register custom post type and helper functions.
 *
 * @package WordPress
 */
/**
 * Register custom post type with custom options
 * @var array
 */
$options = array(
	'public'        => true,
	'menu_position' => 4,
	'supports' => [
		'title',
		'editor',
		'thumbnail'
	],
);

$story = new CustomPostType( 'story', $options );

/**
 * Set menu icon for custom post type
 *
 * @link https://developer.wordpress.org/resource/dashicons/
 */
$story->menu_icon( 'dashicons-media-interactive' );
/**
 * Register new taxonomy with 'story' post type
 */
$story->register_taxonomy( 'type' );
/**
 * Get stories
 *
 * Prepare query for getting stories posts.
 *
 * @param string $type Type taxonomy term
 * @return obj|Error   Returns query object or error
 */
function get_stories( $type = '', $per_page = -1 ) {
	$args = array(
		'post_type'      => 'story',
		'posts_per_page' => $per_page,
		'orderby'        => 'date',
		'order'          => 'DESC',
		'tax_query' => array(
			array(
				'taxonomy' => 'type',
				'field'    => 'slug',
				'terms'    => $type,
			),
		),
	);
	$query = new WP_Query( $args );
	
	return $query;
}
