<?php
/**
 * Jobs custom post type
 *
 * Register custom post type and helper functions.
 *
 * @package WordPress
 */
/**
 * Register custom post type with custom options
 * @var array
 */
$options = array(
	'public'        => true,
	'menu_position' => 5,
	'supports'      => array(
		'title'
	)
);

$jobs = new CustomPostType( 'job', $options );

/**
 * Set menu icon for custom post type
 *
 * @link https://developer.wordpress.org/resource/dashicons/
 */
$jobs->menu_icon( 'dashicons-universal-access-alt' );

/**
 * Get jobs
 *
 * Prepare query for getting job posts.
 *
 * @return obj|Error   Returns query object or error
 */
function get_jobs() {
	$args = array(
		'post_type'      => 'job',
		'posts_per_page' => -1,
		'orderby'        => 'date',
		'order'          => 'DESC'
	);
	$query = new WP_Query( $args );

	return $query;
}
