<?php
/**
 * Press custom post type
 *
 * Register custom post type and helper functions.
 *
 * @package WordPress
 */
/**
 * Register custom post type with custom options
 * @var array
 */
$options = array(
	'public'        => true,
	'menu_position' => 4.9,
	'supports' => [
		'title',
		'editor',
		'thumbnail',
		'comments',
	],
);

$press = new CustomPostType( 'press', $options );

/**
 * Set menu icon for custom post type
 *
 * @link https://developer.wordpress.org/resource/dashicons/
 */
$press->menu_icon( 'dashicons-media-document' );

/**
 * Get press
 *
 * Prepare query for getting press posts ordered by sticky field.
 *
 * @return obj|Error   Returns query object or error
 */
function get_press() {
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

	$args = array(
		'post_type'      => 'press',
		'posts_per_page' => 9,
		'meta_key'		 => 'sticky',
		'orderby'        => 'meta_value_num date',
		'order'          => 'DESC',
		'paged'          => $paged
	);
	$query = new WP_Query( $args );

	return $query;
}
