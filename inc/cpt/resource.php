<?php
/**
 * Resource custom post type
 *
 * Register custom post type and helper functions.
 *
 * @package WordPress
 */
/**
 * Register custom post type with custom options
 * @var array
 */
$options = array(
	'public'        => true,
	'menu_position' => 4,
	'supports' => [
		'title',
		'editor',
		'thumbnail'
	],
);

$resource = new CustomPostType( 'resource', $options );

/**
 * Set menu icon for custom post type
 *
 * @link https://developer.wordpress.org/resource/dashicons/
 */
$resource->menu_icon( 'dashicons-paperclip' );
/**
 * Register new taxonomy with 'resource' post type
 */
$resource->register_taxonomy( 'format' );
$resource->taxonomy_settings['format']['show_in_menu'] = 0;
$resource->taxonomy_settings['format']['show_in_nav_menus'] = 0;
// $resource->taxonomy_settings['format']['show_admin_column'] = 1;
$resource->taxonomy_settings['format']['sort'] = 1;

add_action( 'init', 'insert_format_term', 10, 2 );
add_filter( 'pre_insert_term', 'no_new_type_term', 10, 2 );
/**
 * Insert term for 'format' taxonomy
 *
 * We are hiding 'format' taxonomy UI because we want
 * no user input from client here. Just insert terms and
 * use them from edit post screen. Also, we are using plugin for
 * preventing selecting multiple terms per post.
 *
 * This function is attached to 'init' action hook.
 *
 * @return void
 */
function insert_format_term() {
	wp_insert_term( 'media', __( 'format' ) );
	wp_insert_term( 'gallery', __( 'format' ) );
	wp_insert_term( 'document', __( 'format' ) );
	wp_insert_term( 'form', __( 'format' ) );
	wp_insert_term( 'audio', __( 'format' ) );
}

/**
 * No new terms
 *
 * Prevent adding new terms for 'format' taxonomy. This function is attached to
 * 'pre_insert_term' filter hook.
 *
 * @param  string $term        Taxonomy term name
 * @param  string $taxonomy    Taxonomy name
 * @return array|Error         Returns existing terms or error on attempt to create new term
 */
function no_new_type_term( $term, $taxonomy ) {

	$terms = get_terms( array(
		'taxonomy' => 'format',
		'hide_empty' => false,
	) );

	if ( $taxonomy == 'format' && ! in_array( $term, $terms ) ) {
		return new WP_Error();
	}

	return $term;
}

/**
 * Get resources
 *
 * Prepare query for getting resources.
 *
 * @return obj|Error  Returns query object or error
 */
if ( !function_exists( 'get_resources' ) ) {
	function get_resources() {
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

		$args = array(
			'post_type'      => 'resource',
			'posts_per_page' => 9,
			'orderby'        => 'date',
			'order'          => 'DESC',
			'paged'          => $paged
		);
		$query = new WP_Query( $args );

		return $query;
	}
}