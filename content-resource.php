<?php
/**
 * Resource Content
 *
 * Template for displaying 'resource' cpt content
 *
 * @package WordPress
 */
/**
 * Avoid notices
 * @var string
 */
$type_slug = '';
/**
 * Get resource type taxonomy
 * @var array
 */
$terms = wp_get_post_terms( get_the_ID(), 'format' );
foreach ( $terms as $term ) {
	$type = $term->name;
	$type_slug = $term->slug;
}
/**
 * Names of icons are not the same as categories
 * @var string
 */
if ( $type_slug === 'media' ) {
	$icon = 'film';
} elseif ( $type_slug === 'form' ) {
	$icon = 'forms';
} else {
	$icon = $type_slug;
}
if ( has_post_thumbnail() ) {
	// translators: image size, link to post, echo, image class
	$featured = house_featured_image( 'medium_large', false, false );
} else {
	$featured = house_svg_icon( $icon );
}

/**
 * Get the resource file
 * @var array
 */
$file = get_field( 'resource_file' );
/**
 * Generate popup id from post title
 * @var string
 */
$popup_id = sanitize_title_with_dashes( get_the_title() ); ?>

<div class="layout__item extralarge-and-up-1/3 medium-and-up-1/2 mix category-<?php echo $type_slug; ?>">
	<a href="#resources-<?php the_ID(); ?>" class="pink-box openpopup"><?php echo $featured; ?></a>

	<div class="thumbnail">
		<h3><a href="#resources-<?php the_ID(); ?>" class="openpopup"><?php the_title(); ?></a></h3>
		<p class="subtitle"><?php echo $type_slug; ?></p>
		<p class="gray-text"><?php echo content_trim_words( 20, '..' ); ?></p>
		<hr>

		<div class="justifize">
			<div class="justifize__box">
				<a href="#resources-<?php the_ID(); ?>" class="openpopup more-info">+ SHOW MORE</a>
			</div><!-- justifize__box -->
			<div class="justifize__box">
				<?php if ( $file ) : ?>
					<a href="<?php echo $file; ?>" target="_blank" class="more-info">DOWNLOAD</a>
				<?php endif; // $file ?>
			</div><!-- justifize__box -->
		</div><!-- justifize -->
	</div><!-- thumbnail -->
</div><!-- layout__item extralarge-and-up-1/3 medium-and-up-1/2 mix category-<?php echo $type_slug; ?> -->

<?php
/**
 * Popup content
 */
	/**
	 * Get the gallery
	 * @var array
	 */
	$images = get_field( 'resource_gallery' );
	/**
	 * Get the featured image
	 */
	if ( has_post_thumbnail() ) {
		$featured_img = get_the_post_thumbnail_url( get_the_ID(), 'large' );
	} else {
		$featured_img = '';
	}
?>
<div id="resources-<?php the_ID(); ?>" class="resources-lightbox magnific-popup mfp-hide magnific-animate">
	<div class="resources-lightbox__content">
	<?php
		/**
		 * If we have gallery display slider,
		 * else if we have featured image display it instead of large slider,
		 * else display nothing
		 */
		if ( $images || $featured_img ) : ?>
		<div class="resources__slick-carousel slider-for">
			<?php
				/**
				 * Get the gallery
				 */
				if ( $images ) :
					foreach ( $images as $image ) : ?>
						<div class="resources__slick-carousel--img-large" style="background-image:url('<?php echo $image['url']; ?>')"></div>
					<?php endforeach; // $images as $image
				/**
				 * Get featured image
				 */
				elseif ( $featured_img ) : ?>
					<div class="resources__slick-carousel--img-large" style="background-image:url('<?php echo $featured_img; ?>')"></div>
			<?php endif; // $images ?>
		</div><!-- resources__slick-carousel slider-for -->
		<?php endif; // $images || $featured_img

		/**
		 * If we have gallery build carousel with smaller images
		 */
		if ( $images ) : ?>
		<div class="resources__slick-carousel--thumbnail slider-nav">
			<?php foreach ( $images as $image ) : ?>
				<div class="resources__slick-carousel--img-thumb" style="background-image:url('<?php echo $image['sizes']['medium']; ?>')"></div>
			<?php endforeach; // $images as $image ?>
		</div><!-- resources__slick-carousel--thumbnail slider-nav -->
		<?php endif; // $images ?>

		<div class="resources-lightbox__content-main p+">
			<h3><?php the_title(); ?></h3>
			<?php the_content(); ?>
			<?php if ( $file ) : ?>
				<div class="text-center">
					<a href="<?php echo $file; ?>" target="_blank" class="btn btn--primary">DOWNLOAD</a>
				</div><!-- text-center -->
			<?php endif; // $file ?>
		</div><!-- resources-lightbox__content-main p+ -->
	</div><!-- resources-lightbox__content -->

	<div class="mfp-close">
		<span>close</span> <?php echo house_svg_icon( 'close' ); ?>
	</div><!-- mfp-close -->
</div><!-- resources-lightbox magnific-popup mfp-hide magnific-animate -->