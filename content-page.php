<?php
/**
 * The default template for displaying content.
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="container">
		<?php get_template_part( 'partials/navigations/breadcrumbs'); ?>
	</div>

	<?php
		/**
		 * Get flexible content fields if any,
		 * otherwise get regular content
		 */
		if ( get_field( 'content_fields' ) ) :

			get_template_part( 'partials/flexible/sections' );

		else :

			get_template_part( 'partials/content/post-content' );

		endif; // get_field( 'content_fields' )

	?>

</article><!-- #post -->