<?php
/**
 * Single Story
 *
 * The Template for displaying all single stories.
 *
 * @package WordPress
 */
get_header(); ?>

<div class="main-content">

	<?php while ( have_posts() ) : the_post(); ?>

		<div class="container">
			<?php get_template_part( 'partials/navigations/breadcrumbs'); ?>

			<div class="news-inner__heading">
				<p class="date type-uppercase"><?php echo get_the_date( 'd F Y' ); ?></p>
				<h1><?php the_title(); ?></h1>

				<div class="layout">
					<div class="layout__item 1/2">
						<p class="type-uppercase">by <?php the_author(); ?></p>
					</div><!-- layout__item 1/2 -->
				</div>
			</div><!-- news-inner__heading -->
		</div><!-- container -->

		<?php
			/**
			 * If we have template part for post format and
			 * we are on post format single.
			 *
			 * WordPress will first look for 'format-FORMAT_NAME.php',
			 * if none found fallback is 'format.php'. If that one is missing as well,
			 * it'll look for 'content.php'
			 */
			if ( has_post_format( get_post_format() ) ) {
				get_template_part( 'format', get_post_format() );
			}
			/**
			 * If we have template part for custom post type and
			 * we are on custom post type single.
			 *
			 * WordPress will first look for 'content-CPT_NAME.php',
			 * if none found fallback is 'content.php'.
			 */
			elseif ( post_type_exists( get_post_type() ) ) {
				get_template_part( 'content', get_post_type() );
			}
			/**
			 * If, in any case, none from above applies
			 */
			else {
				get_template_part( 'content' );
			}

			// related news

	endwhile; // end of the loop.

	/**
	 * Get related news
	 */
	get_template_part( 'partials/content/related-news' );
	/**
	 * Get subscribe section
	 */
	get_template_part( 'partials/content/subscribe' ); ?>

</div><!-- main-content -->

<?php get_footer();