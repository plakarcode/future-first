<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 */
get_header();
?>

<article id="post-0" class="post error404 no-results not-found">
	<div class="main-content">
		<div class="container mv">
			<h1>Oops!</h1>
			<p>Looks like the page you're looking for can't be found. Please navigate to a new page via the menu above.</p>
		</div><!-- container -->
	</div><!-- .entry-content -->
</article><!-- #post-0 -->

<?php get_footer(); ?>