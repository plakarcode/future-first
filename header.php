<?php
/**
 * The Header
 *
 * Displays all of the <head> section
 *
 * @todo set logo image
 *
 * @package WordPress
 */
?><!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?> class="<?php house_html_class(); ?> no-js lt-ie9 lt-ie8 lt-ie7 ie6">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?> class="<?php house_html_class(); ?> no-js lt-ie9 lt-ie8 ie7">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?> class="<?php house_html_class(); ?> no-js lt-ie9 ie8">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?> class="<?php house_html_class(); ?> no-js">
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1" />

<!-- Prefetch DNS for external assets -->
<!-- Prefetch Google services -->
<link rel="dns-prefetch" href="//ajax.googleapis.com">
<link rel="dns-prefetch" href="//fonts.googleapis.com">
<link rel="dns-prefetch" href="//themes.googleusercontent.com">
<link rel="dns-prefetch" href="//www.google-analytics.com">
<!-- Prefetch Vimeo player -->
<link rel="dns-prefetch" href="//secure-b.vimeocdn.com">
<link rel="dns-prefetch" href="//player.vimeo.com">
<link rel="dns-prefetch" href="//i.vimeocdn.com">
<!-- Prefetch Facebook plugins -->
<link rel="dns-prefetch" href="//www.facebook.com">
<link rel="dns-prefetch" href="//connect.facebook.net">
<link rel="dns-prefetch" href="//static.ak.facebook.com">
<link rel="dns-prefetch" href="//static.ak.fbcdn.net">
<link rel="dns-prefetch" href="//s-static.ak.facebook.com">
<!-- Prefetch Twitter -->
<link rel="dns-prefetch" href="//platform.twitter.com">
<link rel="dns-prefetch" href="//p.twitter.com">
<link rel="dns-prefetch" href="//cdn.api.twitter.com">

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" /><?php
global $wp, $globalSite;
$current_url = home_url( add_query_arg( array(), $wp->request ) . '/');
$sitename = get_bloginfo( 'name' );
$sitedescription = get_bloginfo( 'description' );
$type = '';
if ( is_front_page() ) {
	$type = 'website';
} else {
	$type = 'article';
} ?>
<script type="application/ld+json">
{
	"@context": "http://schema.org",
	"@type": "Organization",
	"name": "<?php echo $sitename; ?>",
	"description": "<?php echo html_entity_decode( $sitedescription ); ?>",
	"url": "<?php echo $globalSite['home']; ?>",
	"logo": "<?php echo $globalSite['template_url']; ?>/images/logo.jpg"
}
</script>
<?php wp_head(); ?>
</head>

<body id="top" <?php body_class(); ?> >
	<div id="preloader"></div><!-- /#preloader -->
	<!--[if lt IE 9]>
		<div class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->

	<header id="mainheader" class="header-main" role="banner">
		<div class="header-main__top">
			<div class="justifize justifize--middle">
				<div class="justifize__box">
					<?php
						/**
						 * Get social links
						 */
						get_template_part( 'partials/header/social' );
					?>
				</div><!-- justifize__box -->

				<div class="justifize__box header-main__top--content">
					<?php
						/**
						 * Get contact info
						 */
						get_template_part( 'partials/header/contact' );
					?>
				</div><!-- justifize__box header-main__top--content -->
			</div><!-- justifize justifize--middle -->
		</div><!-- header-main__top -->

		<div class="header-main__content-extralarge">
			<div class="justifize justifize--middle">
				<div class="justifize__box">
					<a href="/">
						<?php echo house_svg_icon( 'logo' ); ?>
					</a>
					<a href="/">
						<?php echo house_svg_icon( 'logomark' ); ?>
					</a>
				</div><!-- justifize__box -->

				<div class="justifize__box">
					<?php
						/**
						 * Get main naivation
						 */
						get_template_part( 'partials/navigations/main' );
					?>
				</div><!-- justifize__box -->
			</div><!-- justifize justifize--middle -->
		</div><!-- header-main__bottom -->

		<div class="header-main__content-large">
		    <div class="header-main__nav-toggle">
			    <div class="justifize">
			        <div class="justifize__box">
			          	<a href="/" class="mobile-logo">
							<?php echo house_svg_icon( 'logo' ); ?>
						</a>
		        	</div>

		        	<div class="justifize__box searchform-wrap">
		          		<a href="javascript:;" id="menu-toggle--close" class="hidden">
		  					<svg role="img" class="icon ">
		    					<use xlink:href="<?php echo get_template_directory_uri(); ?>/icons/icons.svg#icon-close"></use>
		  					</svg>
						</a>
		          		<a href="javascript:;" id="menu-toggle--show">
		  					<svg role="img" class="icon icon-menu">
		    					<use xlink:href="<?php echo get_template_directory_uri(); ?>/icons/icons.svg#icon-menu"></use>
		  					</svg>
						</a>
		        	</div>
		      	</div>
		    </div><!-- header-main__nav-toggle -->

		    <div class="header-main__nav-content header-main__nav-content-large">
		      	<?php
					/**
					 * Get main naivation
					 */
					get_template_part( 'partials/navigations/main' );
				?>
		    </div><!-- header-main__nav-content header-main__nav-content-large -->
	  	</div><!-- header-main__content-large -->
	</header><!-- #mainheader -->
